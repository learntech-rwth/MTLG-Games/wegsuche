MTLG.lang.define({
  'en': {
    'einSpieler': 'One Player',
    'zweiSpieler': 'Two Players',
    'titel-Login': 'Type in User and password', 
    'Titel_Feld1': 'Take the rectangle to the white area',
    'Titel_Feld2': 'Press the fields with the numbers in the right order...',
    'temp_feedback': 'Your needed time in milliseconds is: ',
    'feedback_text': 'ms needed',
    'Demo Game' : 'Demo Game',
    'Start!' : 'Start!',
  },
  'de': {
    'einSpieler': 'Ein Spieler', 
    'zweiSpieler': 'Zwei Spieler',
    'titel-Login': 'Bitte Namen und Passwort eingeben', 
    'Titel_Feld1': 'Ziehe das Rechteck auf das weiße Feld',
    'Titel_Feld2': 'Drücke die Nummern in der richtigen Reihenfolge...',
    'temp_feedback': 'Deine benötigte Zeit (in ms) beträgt: ',
    'feedback_text': 'ms gebraucht',
    'Demo Game' : 'Demo Game',
    'Start!' : 'Start!',
  }
});
