//knotenanzahl: 11
var kantenBer;
function berechneKantenTeam(){
	/*
		Initialisiere zweidimensionales Array um Kantengewichte zu speichern
	*/
	
	kantenBer = new Array(11);
	for(i = 0; i < 11; i++) {
		kantenBer[i] = new Array(11);
	}
	
	/*
		Setze zunaechst alle Kanten auf 0
	*/
	
	for(j = 0; j < 11; j++) {
		for(k = 0; k < 11; k++) {
			kantenBer[j][k] = 0;
		}
	}
	
	/*
		Setze ausgewählte Kanten auf zufällig gewählte Gewichte
	*/
	
	kantenBer[0][3] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[0][2] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[1][2] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[1][3] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[1][10] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[2][5] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[3][6] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[4][6] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[4][7] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[4][10] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[5][7] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[5][8] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[5][9] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[5][10] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[7][10] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[8][9] = parseInt(Math.ceil(Math.random() * 15) + 1);
	
	/*
		Die Kanten sollen ungerichtet sein, werden allerdings gerichtet gespeichert, sodass alle Werte "gespiegelt" werden muessen.
	*/
	
	for(l = 0; l < 11; l++){
		for(m = 0; m < 11; m++){
			if(l != m) {
				if(!(kantenBer[l][m] == 0 && kantenBer[m][l] == 0)) {
					if(l > m) {
						kantenBer[l][m] = kantenBer[m][l];
					}
				}
			}
		}
	}
}

function getKanten() {
	berechneKantenTeam();
	for(i = 0; i < 11; i++){
		for(j = 0; j < 11; j++){
			localStorage.setItem("kanten" + i+ "_" + j, kantenBer[i][j]);
		}
	}
	return kantenBer;
}