var stage;

function resetGame() {
  setDefaultValues();
  MTLG.lc.goToMenu();
}

function goToFeedback() {
  stage.removeAllChildren();
  stage.removeAllEventListeners();
  feedback(stage);
}

//Register Init function with the MTLG framework
MTLG.addGameInit((pOptions) => {
  //set globals
  stage = MTLG.gibStage();
  stage.enableMouseOver(20);
  setDefaultValues();
  skalierung();

  MTLG.lc.registerMenu(auswahlFrame);
  MTLG.lc.registerLevel(callspielfeld, ()=>1);
});
