var kantenBer;

function berechneKantengewicht() {
	knotenanzahl = 29;

	/*
		Initialisiere zweidimensionales Array um Kantengewichte zu speichern
	*/
	
	kantenBer = new Array(knotenanzahl);
	for(i = 0; i < kantenBer.length; i++) {
		kantenBer[i] = new Array(knotenanzahl);
	}
	
	/*
		Setze zunaechst alle Kanten auf 0
	*/
	
	for(j = 0; j < kantenBer.length; j++){
		for(k = 0; k < kantenBer.length; k++){
			kantenBer[j][k] = 0;
		}
	}
	
	/*
		Setze ausgewählte Kanten auf zufällig gewählte Gewichte
	*/
	
	kantenBer[0][1] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[0][14] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[1][2] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[1][14] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[2][3] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[2][13] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[2][15] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[3][11] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[3][13] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[4][5] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[4][11] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[4][18] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[5][6] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[5][10] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[6][7] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[6][20] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[7][8] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[8][20] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[8][21] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[9][19] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[9][20] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[10][18] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[10][19] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[11][12] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[12][13] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[12][18] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[13][16] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[13][17] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[13][25] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[14][15] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[15][16] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[15][28] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[17][26] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[17][27] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[17][28] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[18][25] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[19][24] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[19][25] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[20][23] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[21][22] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[22][23] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[23][24] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[25][26] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[26][27] = parseInt(Math.ceil(Math.random() * 15) + 1);
	kantenBer[27][28] = parseInt(Math.ceil(Math.random() * 15) + 1);

	
	/*
		Die Kanten sollen ungerichtet sein, werden allerdings gerichtet gespeichert, sodass alle Werte "gespiegelt" werden muessen.
	*/
	
	for(l = 0; l < knotenanzahl; l++){
		for(m = 0; m < knotenanzahl; m++){
			if(l != m) {
				if(!(kantenBer[l][m] == 0 && kantenBer[m][l] == 0)) {
					if(l > m) {
						kantenBer[l][m] = kantenBer[m][l];
					}
				}
			}
		}
	}
}

function getKantenWett() {
	berechneKantengewicht();
	for(i = 0; i < knotenanzahl; i++){
		for(j = 0; j < knotenanzahl; j++){
			localStorage.setItem("kanten" + i+ "_" + j, kantenBer[i][j]);
		}
	}
	return kantenBer;
}