function skH(y){
	//return y;
	return (y * window.screen.height / 1050);
}

function skB(x){
	//return x;
	return x * window.screen.width / 1920;
}


/*
	Methode, die das Verhältnis zwischen gegangenem Weg und dem kürzesten Weg berechnet
	und dementsprechend die Anzahl der verdienten Sterne ermittelt
*/

function rating(gegangenerWeg, kuerzester) {
	var rating = gegangenerWeg/kuerzester;
	var anzahlSterne = 0;
	if(rating >= 1) {
		if(rating == 1) {
		anzahlSterne = 5;
		} else if(rating > 1 && rating <= 1.2) {
			anzahlSterne = 4;
		} else if(rating > 1.2 && rating <= 1.5) {
			anzahlSterne = 3;
		} else if(rating > 1.5 && rating <= 1.8) {
			anzahlSterne = 2;
		} else {
			anzahlSterne = 1;
		}
	} else {
		alert("Fehler in der Berechnung der Quote aufgetreten!");
		throw new Error();
	}
	return anzahlSterne;
}


/*
	Funktion um Farbe ins englische zu übersetzen
*/
function color(farbe){
	switch(farbe){
		case "gruen":
			return "green";
			break;
		case "gelb":
			return "yellow";
			break;
		case "blau":
			return "blue";
			break;
		case "rot":
			return "red";
			break;
		default:
			return "white";
			break;
	}
}
/*
	Methoden um Figuren zu erstellen
*/
function createFrame(colorStroke, colorFill, posX, posY, width, height) {
	frame_shape = new createjs.Shape();
	frame_shape.graphics.beginStroke(colorStroke).beginFill(colorFill).drawRoundRect(0,0,width,height, 10);
	frame_shape.x = posX;
	frame_shape.y = posY;
	return frame_shape;
}

function createText(content, colorText, posX, posY) {
	var text_shape = new createjs.Text(content, "30px Arial", colorText);
	text_shape.x = posX;
	text_shape.y = posY;
	return text_shape;
}

function createTextGroesse(content, colorText, posX, posY, groesse) {
	var text_shape = new createjs.Text(content, groesse + "px Arial", colorText);
	text_shape.x = posX;
	text_shape.y = posY;
	return text_shape;
}

function createBitmap(bitSource, posX, posY) {
	var bitmap_shape = MTLG.assets.getBitmap(bitSource);
	bitmap_shape.x = posX;
	bitmap_shape.y = posY;
	return bitmap_shape;
}

function createPolystar(x,y,radius, sides, pointSize, angle, fillColor) {
	var poly = new createjs.Shape();
    poly.graphics.setStrokeStyle(4,"square").beginStroke("black");
    poly.graphics.beginFill(fillColor);
    poly.graphics.drawPolyStar(0, 0, radius, sides, pointSize, angle);
    poly.x = x;
    poly.y = y;
    return poly;
}

function createPolystarSmall(x,y,radius, sides, pointSize, angle, fillColor) {
	var poly = new createjs.Shape();
    poly.graphics.setStrokeStyle(1,"square").beginStroke("black");
    poly.graphics.beginFill(fillColor);
    poly.graphics.drawPolyStar(0, 0, radius, sides, pointSize, angle);
    poly.x = x;
    poly.y = y;
    return poly;
}

function createButton(strokeColor, fillColor, width, height, radius, posx, posy) {
	var button_shape = new createjs.Shape();
	button_shape.graphics.beginStroke("red").beginFill("yellow").drawRoundRect(0,0,400,150, 10);
	button_shape.x = posx;
	button_shape.y = posy;
	return button_shape;
}

function createLine(strokeColor, moveToX, moveToY, lineToX, lineToY) {
	var line_shape = new createjs.Shape();
	line_shape.graphics.beginStroke(strokeColor);
	line_shape.graphics.setStrokeStyle(4);
	line_shape.graphics.moveTo(moveToX, moveToY);
	line_shape.graphics.lineTo(lineToX, lineToY);
	return line_shape;
}
