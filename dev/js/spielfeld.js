﻿/*
	Beim Aufruf dieser Methode wird je nach Auswahl der Modi das entsprechende Spielfeld ausgewählt
*/

function callspielfeld() {
	var spielModus = localStorage.getItem("spielModus");
	var spielerAnzahl = localStorage.getItem("spielerAnzahl");

	if(spielModus == "team" || spielerAnzahl == 1) {
		spielfeldteam(stage);
	} else {
		if(spielModus == "gegeneinander") {
			spielfeldwett(stage);
		} else {
			throw new Error();
		}
	}
}
