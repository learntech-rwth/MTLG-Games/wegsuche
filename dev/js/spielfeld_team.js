var kastenFarbe;
var schattenFarbe;
var rahmenFarbe;
var knotenanzahl;
var kantengewichtFarbe;
var kantenFarbe;
var box;
var rahmen;
var zielFlaggen;
var width;
var height;
var start;
var ziel;
var spieler;
var spielerFarbe;
var spielFigur;
var kanten;
var textWeglaenge;
var vor;
var zurueck;
var kantenReihenfolge;
var linien;
var nochmalTeam;
var screenWidth;
var screenHeight;

/*
	Diese Methode initiasiert die Werte der oben deklarierten Variable
*/

function preuse_spielfeldTeam() {
	screenWidth = localStorage.getItem("screenWidth");
	screenHeight = localStorage.getItem("screenHeight");
	kastenFarbe = localStorage.getItem("kastenFarbe");
	schattenFarbe = localStorage.getItem("schattenFarbe");
	rahmenFarbe = localStorage.getItem("rahmenFarbe");
	knotenanzahl = localStorage.getItem("knotenAnzahl");
	box = new Array(knotenanzahl);
	rahmen = new Array(knotenanzahl);
	width = parseInt(localStorage.getItem("width"));
	height = parseInt(localStorage.getItem("height"));
	nochmalTeam = parseInt(localStorage.getItem("nochmalTeam"));
	start = new Array(1);
	ziel = new Array(1);
	if(nochmalTeam == 1){
		start[0] = localStorage.getItem("start0");
		ziel[0] = localStorage.getItem("ziel0");
	}
	spielerFarbe = localStorage.getItem("chosenFarbe0");
	spielFigur = localStorage.getItem("chosenSpieler0");
	kantengewichtFarbe = localStorage.getItem("kantengewichtFarbe");
	kantenFarbe = localStorage.getItem("kantenFarbe");
}

/*
	Baut das Spielfeld auf
*/

function spielfeldteam(stage) {
	preuse_spielfeldTeam();
	baueSpielfeldTeam(stage);

	var zaehler = 0;

	var hintergrund = new createjs.Shape();
	hintergrund.graphics.beginStroke("white").beginFill("white").drawRoundRect(0,0,1920,1080, 10);
	hintergrund.height = screenHeight;
	hintergrund.width = screenWidth;

	hintergrund.alpha = 0.5;

	var neustartButton = createFrame(localStorage.getItem("rahmenFarbe"), "brown", 0.45 * screenWidth, 0.45 * screenHeight, 300, 150);
	var neustartText = createText("Neustart", "black", neustartButton.x + 100, neustartButton.y + 50);

	hintergrund.on("click", function(event){
		stage.removeChild(neustartButton, neustartText, hintergrund);
		zaehler = 0;
		neustart = 0;
	});

	neustartButton.addEventListener("pressup", function(event) {
		resetGame();
	});
	var neustart = 0;

	createjs.Ticker.addEventListener("tick", neustartTicker);
	function neustartTicker(event) {
		if(zaehler < localStorage.getItem("timeout") ){
			zaehler ++;
		} else  if(neustart == 0){
			stage.addChild(hintergrund);
			stage.addChild(neustartButton);
			stage.addChild(neustartText);
			neustart = 1;
		}
	}

	var aktWeg = 0;

	//Array, das festhält, welche Kante schon ausgewählt wurde
	var chosenKante = new Array(knotenanzahl);
	for(i = 0; i < knotenanzahl; i++) {
		chosenKante[i] = new Array(knotenanzahl);
	}

	for(i = 0; i < knotenanzahl; i++) {
		for(j = 0; j < knotenanzahl; j++) {
			chosenKante[i][j] = false;
		}
 	}

//--------------------------------------------------------------------------------------------------------------------
//Das Spiel startet

/*
Das Spielerarray wird initialisiert und sein Bitmap in den Startknoten gelegt.
*/	spieler = new Array(1);

	for(i = 0; i < spieler.length; i++) {
		//Spieler wird angelegt
		spieler[i] = new Array();
		//aktuelle Position
		spieler[i][2] = start[i];
		//Zielposition
		spieler[i][3] = ziel[i];
		//speichert vom entsprechenden Spieler besuchten Knoten
		spieler[i][4] = new Array();
		//Den Startknoten des jeweiligen Spielers initial zu den besuchten Knoten zählen
		spieler[i][4][0] = start[i];
		//Anzahl besuchter Knoten
		spieler[i][5] = 1;
		//speichert die aktuelle Weglänge
		spieler[i][6] = 0;
		//speichert zuletzt besuchten Knoten
		spieler[i][7] = 0;
	}

	var bitmapSpieler = createBitmap("img/Figuren/spielfigur_"+(parseInt(spielFigur)+1)+"_"+ spielerFarbe+".png", box[start[0]].x - 30, box[start[0]].y - 50);
	bitmapSpieler.visible = true;
	stage.addChild(bitmapSpieler);

	/*
		Event-Funktion, um die Spielfigur zu bewegen
	*/

	bitmapSpieler.on("pressmove", function(event) {
		zaehler = 0;
		event.target.x = event.stageX;
		event.target.y = event.stageY;
	});

	//speichert den Vorgaenger
	var pre = -1;

	/*
		Eventhandler, wenn die Spielfigur auf dem Spielfed abgelassen wird
	*/

	bitmapSpieler.on("pressup", function(event) {
		zaehler = 0;
		var value = aufKnoten(bitmapSpieler);

		/*
			Wenn die Spielfigur nicht auf einem Knoten abgelassen wird
		*/

		if(value == -1 || kanten[spieler[0][2]][value] < 1) {
			//setze Koordinaten des Bitmap wieder auf vorherigen Knoten
			bitmapSpieler.x = box[spieler[0][2]].x - 30;
			bitmapSpieler.y = box[spieler[0][2]].y - 50;
		} else{
			//Anfangsschritt
			if(pre == -1 && !chosenKante[spieler[0][2]][value] && !chosenKante[value][spieler[0][2]]) {
				bitmapSpieler.x = box[value].x - 30;
				bitmapSpieler.y = box[value].y - 50;
				spieler[0][6] = spieler[0][6] + kanten[spieler[0][2]][value];
				chosenKante[spieler[0][2]][value] = true;
				textWeglaenge.text = "aktuelle Weglänge:" + spieler[0][6];
				colorLine(spieler[0][2], value, color(spielerFarbe));
				pre = spieler[0][2];
				spieler[0][2] = value;
				spieler[0][5]++;
				spieler[0][4].push(value);
			/*
				Wenn der Spieler die Kante schon gelaufen ist und die Kante zurückgesetzt wird
			*/
			} else if(pre == value && !chosenKante[spieler[0][2]][value] && chosenKante[value][spieler[0][2]]) {
				bitmapSpieler.x = box[value].x - 30;
				bitmapSpieler.y = box[value].y - 50;
				spieler[0][6] = spieler[0][6] - kanten[spieler[0][2]][value];
				chosenKante[spieler[0][2]][value] = true;
				chosenKante[value][spieler[0][2]] = false; // Damit die Figur die Strecke nicht direkt zurücklaufen kann
				textWeglaenge.text = "aktuelle Weglänge:" + spieler[0][6];
				colorLine(spieler[0][2], value, kantenFarbe);
				spieler[0][2] = value;
				spieler[0][5]--;
				pre = spieler[0][4][spieler[0][5] - 2];
				spieler[0][4].pop();
			/*
				Wenn der Spieler schonmal bewegt wurde und eine noch nicht ausgewählte Kante läuft
			*/
			} else {
				bitmapSpieler.x = box[value].x - 30;
				bitmapSpieler.y = box[value].y - 50;
				spieler[0][6] = spieler[0][6] + kanten[spieler[0][2]][value];
				chosenKante[spieler[0][2]][value] = true;
				chosenKante[value][spieler[0][2]] = false; // Damit die Figur die Strecke nicht direkt zurücklaufen kann
				textWeglaenge.text = "aktuelle Weglänge:" + spieler[0][6];
				colorLine(spieler[0][2], value, color(spielerFarbe));
				pre = spieler[0][2];
				spieler[0][2] = value;
				spieler[0][5]++;
				spieler[0][4].push(value);
			}
		}
	});

	/*
		Methode, um die Kanten zu färben
		muss in der Funktion liegen und nicht ausserhalb, da sie sonst stage nicht kennt!
	*/

	function colorLine(node1, node2, lineColor) {
		var index = -1;
		if(!((kantenReihenfolge[node1][node2] == undefined) && (kantenReihenfolge[node2][node1] == undefined))) {
			if(kantenReihenfolge[node1][node2] == undefined) {
				index = kantenReihenfolge[node2][node1];
			} else {
				index = kantenReihenfolge[node1][node2];
			}
			mxa = box[node1].x - 30 + width/2;
			mya = box[node1].y - 50 + height/2;
			mxb = box[node2].x - 30 + width/2;
			myb = box[node2].y - 50 + height/2;
			stage.removeChild(linien[index]);
			linien[index] = createLine(lineColor, mxa, mya, mxb, myb);
			stage.addChild(linien[index]);
		}
	}
}
/*
	Funktion die überprüft, ob Bitmap auf einem knoten liegt.
*/
function aufKnoten(bitmap){
	for(var u = 0; u < knotenanzahl; u++) {
		if(bitmap.x >= (box[u].x - width) && bitmap.x  <= (box[u].x + width) && bitmap.y >= (box[u].y  - height) && bitmap.y <= (box[u].y + height)){
			return u;
		}
	}
	//Falls, Spieler auf keinem bekannten Knoten liegt
	return -1;
}

/*
	In dieser Methode werden die Start und Endknoten zufälig generiert
*/

function setzeStartZiel(){
	var fall = parseInt(Math.ceil(Math.random() * 3));
	start = new Array(1);
	ziel = new Array(1);
	switch(fall) {
		case 0:
			//von Schule nach Hause
			start[0] = 0;
			ziel[0] = 7;
			break;
		case 1:
			//von Musikschule zum Sportplatz
			start[0] = 6;
			ziel[0] = 8;
			break;
		default:
			//von Zu Hause zur Eisdiele
			start[0] = 7;
			ziel[0] = 2;
			break;
	}
	localStorage.setItem("start0", start);
	localStorage.setItem("ziel0", ziel);
}

/*
	Methode, um den Namen der dazugehörigen Knoten zu ermitteln
*/

function knotenName(punkt){
	punkt_content = -1;
	switch(parseInt(punkt)) {
		case 0:
			punkt_content = "Schule";
			break;
		case 1:
			punkt_content = "Kino";
			break;
		case 2:
			punkt_content = "Eisdiele";
			break;
		case 4:
			punkt_content = "Spielplatz";
			break;
		case 6:
			punkt_content = "Musikschule";
			break;
		case 7:
			punkt_content = "Zu Hause";
			break;
		case 8:
			punkt_content = "Sportplatz";
			break;
		default:
			punkt_content = "undefined";
			break;
	}
	return punkt_content;
}

function knotenArtikel(punkt){
	punkt_content = -1;
	switch(parseInt(punkt)) {
		case 0:
			punkt_content = "zu der Schule";
			break;
		case 1:
			punkt_content = "zu dem Kino";
			break;
		case 2:
			punkt_content = "zu der Eisdiele";
			break;
		case 4:
			punkt_content = "zu dem Spielplatz";
			break;
		case 6:
			punkt_content = "zu der Musikschule";
			break;
		case 7:
			punkt_content = "nach Hause";
			break;
		case 8:
			punkt_content = "zu dem Sportplatz";
			break;
		default:
			punkt_content = "undefined";
			break;
	}
	return punkt_content;
}

/*
	Diese Methode baut das Spielfeld samt den dazugehörigen Knoten auf
*/

function baueSpielfeldTeam(stage){
	var textKanten;
	if(nochmalTeam != 1){
		setzeStartZiel();
	}


	/*
		Erzeugt Array mit 11 Knotenpunkten
	*/

	knotenanzahl = 11;
	localStorage.setItem("knotenAnzahl", knotenanzahl);

	/*
		box 0 + schule
	*/

	rahmen[0] = new createjs.Shape();
	if(start[0] == 0){
		rahmen[0].graphics.beginStroke(rahmenFarbe).beginFill(color(spielerFarbe)).drawRoundRect(0,0,2*width+80,2*height, 10);
	} else{
		rahmen[0].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,2*width+80,2*height, 10);
	}
	rahmen[0].x = screenWidth * 0.005;
	rahmen[0].y = screenHeight * 0.009;
	stage.addChild(rahmen[0]);

	box[0] = new createjs.Shape();
	box[0].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[0].x = screenWidth * 0.07;
	box[0].y = screenHeight * 0.23;
	box[0].setBounds(box[0].x, box[0].y, 100, 60);
	stage.addChild(box[0]);

	var schule = new createjs.Bitmap("img/Figuren/spielfeld/schule.png");
	schule.x = screenWidth * 0.015625;
	schule.y = screenHeight * 0.05;
	schule.visible = true;
	stage.addChild(schule);

	var schuleT = new createjs.Text("SCHULE", "18px Arial", "black");
	schuleT.x = screenWidth * 0.025;
	schuleT.y = screenHeight * 0.0185;
	stage.addChild(schuleT);

	/*
		box 1 + kino
	*/

	rahmen[1] = new createjs.Shape();
	rahmen[1].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,2*width+80,2*height+50, 10);
	rahmen[1].x = 0.26 * screenWidth;
	rahmen[1].y = 0.385 * screenHeight;
	stage.addChild(rahmen[1]);

	var kino = new createjs.Bitmap("img/Figuren/spielfeld/kino.png");
	kino.x = 0.27 * screenWidth;
	kino.y = 0.64 * screenHeight;
	kino.visible = true;
	stage.addChild(kino);

	var kinoT = new createjs.Text("KINO", "18px Arial", "black");
	kinoT.x = 0.27 * screenWidth;
	kinoT.y = 0.395 * screenHeight;
	stage.addChild(kinoT);

	box[1] = new createjs.Shape();
	box[1].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[1].x = 0.32 * screenWidth;
	box[1].y = 0.485 * screenHeight;
	box[1].setBounds(box[1].x, box[1].y, 100, 60);
	stage.addChild(box[1]);

	/*
		box 2 + Eisdiele
	*/

	rahmen[2] = new createjs.Shape();
	if(ziel[0] == 2){
		rahmen[2].graphics.beginStroke(rahmenFarbe).beginFill(color(spielerFarbe)).drawRoundRect(0,0,2*width,2*height - 120, 10);
	} else {
		rahmen[2].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,2*width,2*height - 120, 10);
	}
	rahmen[2].x = 0.21 * screenWidth;
	rahmen[2].y = 0.06 * screenHeight;
	stage.addChild(rahmen[2]);

	box[2] = new createjs.Shape();
	box[2].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[2].x = 0.25 * screenWidth;
	box[2].y = 0.148 * screenHeight;
	box[2].setBounds(box[2].x, box[2].y, 100, 60);
	stage.addChild(box[2]);

	var eisdiele = new createjs.Bitmap("img/Figuren/spielfeld/eisdiele.png");
	eisdiele.x = 0.3 * screenWidth;
	eisdiele.y = 0.13 * screenHeight;
	eisdiele.visible = true;
	stage.addChild(eisdiele);

	var eisdieleT = new createjs.Text("EISDIELE", "18px Arial", "black");
	eisdieleT.x = 0.244 * screenWidth;
	eisdieleT.y = 0.07 * screenHeight;
	stage.addChild(eisdieleT);

	/*
		box 3 + Rahmen
	*/

	rahmen[3] = new createjs.Shape();
	rahmen[3].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,2*width,2*height - 120, 10);
	rahmen[3].x = 0.02 * screenWidth;
	rahmen[3].y = 0.418 * screenHeight;
	stage.addChild(rahmen[3]);

	box[3] = new createjs.Shape();
	box[3].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[3].x = 0.07 * screenWidth;
	box[3].y = 0.508 * screenHeight;
	box[3].setBounds(box[3].x, box[3].y, 100, 60);
	stage.addChild(box[3]);

	/*
		Box 4 + Spielplatz
	*/

	rahmen[4] = new createjs.Shape();
	rahmen[4].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,2*width + 200,2*height - 120, 10);
	rahmen[4].x = 0.5 * screenWidth;
	rahmen[4].y = 0.68 * screenHeight;
	stage.addChild(rahmen[4]);

	box[4] = new createjs.Shape();
	box[4].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[4].x = 0.54 * screenWidth;
	box[4].y = 0.77 * screenHeight;
	box[4].setBounds(box[4].x, box[4].y, 100, 60);
	stage.addChild(box[4]);

	var spielplatz = new createjs.Bitmap("img/Figuren/spielfeld/spielplatz.png");
	spielplatz.x = 0.64 * screenWidth;
	spielplatz.y = 0.71 * screenHeight;
	spielplatz.visible = true;
	stage.addChild(spielplatz);

	var spielPlatzT = new createjs.Text("SPIELPLATZ", "18px Arial", "black");
	spielPlatzT.x = 0.51 * screenWidth;
	spielPlatzT.y = 0.69 * screenHeight;
	stage.addChild(spielPlatzT);

	/*
		Box 5 + Rahmen
	*/

	rahmen[5] = new createjs.Shape();
	rahmen[5].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,2*width,2*height - 120, 10);
	rahmen[5].x = 0.45 * screenWidth;
	rahmen[5].y = 0.01 * screenHeight;
	stage.addChild(rahmen[5]);

	box[5] = new createjs.Shape();
	box[5].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[5].x = 0.5 * screenWidth;
	box[5].y = 0.1 * screenHeight;
	box[5].setBounds(box[5].x, box[5].y, 100, 60);
	stage.addChild(box[5]);

	/*
		Box 6 + Musikschule
	*/

	rahmen[6] = new createjs.Shape();
	if(start[0] == 6){
		rahmen[6].graphics.beginStroke(rahmenFarbe).beginFill(color(spielerFarbe)).drawRoundRect(0,0,2*width + 150,2*height - 150, 10);
	} else {
		rahmen[6].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,2*width + 150,2*height - 150, 10);
	}
	rahmen[6].x = 0.01* screenWidth;
	rahmen[6].y = 0.73 * screenHeight;
	stage.addChild(rahmen[6]);

	box[6] = new createjs.Shape();
	box[6].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[6].x = 0.035 * screenWidth;
	box[6].y = 0.8 * screenHeight;
	box[6].setBounds(box[6].x, box[6].y, 100, 60);
	stage.addChild(box[6]);

	var musikschule = new createjs.Bitmap("img/Figuren/spielfeld/musikschule.png");
	musikschule.x = 0.125 * screenWidth;
	musikschule.y = 0.8*screenHeight;
	musikschule.visible = true;
	stage.addChild(musikschule);

	var musikschuleT = new createjs.Text("MUSIKSCHULE", "18px Arial", "black");
	musikschuleT.x = 0.125 * screenWidth;
	musikschuleT.y = 0.8 * screenHeight;
	stage.addChild(musikschuleT);

	/*
		Box 7 + zu Hause
	*/

	rahmen[7] = new createjs.Shape();
	if(start[0] == 7){
		rahmen[7].graphics.beginStroke(rahmenFarbe).beginFill(color(spielerFarbe)).drawRoundRect(0,0,2*width + 120,2*height - 60, 10);
	} else if(ziel[0] == 7) {
		rahmen[7].graphics.beginStroke(rahmenFarbe).beginFill(color(spielerFarbe)).drawRoundRect(0,0,2*width + 120,2*height - 60, 10);
	} else {
		rahmen[7].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,2*width + 120,2*height - 60, 10);
	}
	rahmen[7].x = 0.805 * screenWidth;
	rahmen[7].y = 0.65*screenHeight;
	stage.addChild(rahmen[7]);

	box[7] = new createjs.Shape();
	box[7].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[7].x = 0.83 * screenWidth;
	box[7].y = 0.77 * screenHeight;
	box[7].setBounds(box[7].x, box[7].y, 100, 60);
	stage.addChild(box[7]);

	var zuHause = new createjs.Bitmap("img/Figuren/spielfeld/zuHause.png");
	zuHause.x = 0.89 * screenWidth;
	zuHause.y = 0.69 * screenHeight;
	zuHause.visible = true;
	stage.addChild(zuHause);

	var zuHauseT = new createjs.Text("ZU HAUSE", "18px Arial", "black");
	zuHauseT.x = 0.85 * screenWidth;
	zuHauseT.y = 0.68 * screenHeight;
	stage.addChild(zuHauseT);

	/*
		Box 8 - Sportplatz
	*/

	rahmen[8] = new createjs.Shape();
	if(ziel[0] == 8){
		rahmen[8].graphics.beginStroke(rahmenFarbe).beginFill(color(spielerFarbe)).drawRoundRect(0,0,2.5*width + 200,1.4*height, 10);
	} else {
		rahmen[8].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,2.5*width + 200,1.4*height, 10);
	}
	rahmen[8].x = 0.71 * screenWidth;
	rahmen[8].y = 0.005 *screenHeight;
	stage.addChild(rahmen[8]);

	box[8] = new createjs.Shape();
	box[8].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[8].x = 0.736 * screenWidth;
	box[8].y = 0.07 * screenHeight;
	box[8].setBounds(box[8].x, box[8].y, 100, 60);
	stage.addChild(box[8]);

	var sportplatz = new createjs.Bitmap("img/Figuren/spielfeld/sportplatz.png");
	sportplatz.x = 0.8 * screenWidth;
	sportplatz.y = 0.1 * screenHeight;
	sportplatz.visible = true;
	stage.addChild(sportplatz);

	var sportplatzT = new createjs.Text("SPORTPLATZ", "18px Arial", "black");
	sportplatzT.x = 0.8 * screenWidth;
	sportplatzT.y = 0.01 * screenHeight;
	stage.addChild(sportplatzT);

	/*
		Box 9 - Rahmen
	*/

	rahmen[9] = new createjs.Shape();
	rahmen[9].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,2*width-50,2*height-150, 10);
	rahmen[9].x = 0.72 * screenWidth;
	rahmen[9].y = 0.295 * screenHeight;
	stage.addChild(rahmen[9]);

	box[9] = new createjs.Shape();
	box[9].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[9].x = 0.753 * screenWidth;
	box[9].y = 0.35 * screenHeight;
	box[9].setBounds(box[9].x, box[9].y, 100, 60);
	stage.addChild(box[9]);

	/*
		Box 10 - Rahmen
	*/

	rahmen[10] = new createjs.Shape();
	rahmen[10].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,2*width,2*height-100, 10);
	rahmen[10].x = 0.46 * screenWidth;
	rahmen[10].y = 0.32* screenHeight;
	stage.addChild(rahmen[10]);

	box[10] = new createjs.Shape();
	box[10].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[10].x = 0.5 * screenWidth;
	box[10].y = 0.43 * screenHeight;
	box[10].setBounds(box[10].x, box[10].y, 100, 60);
	stage.addChild(box[10]);

	/*
		Einfuegen der Zielflagge
	*/

	zielFlaggen = new createjs.Bitmap("img/Graphik/zielflagge.png");
	zielFlaggen.x = box[ziel[0]].x-30;
	zielFlaggen.y = box[ziel[0]].y-50;
	stage.addChild(zielFlaggen);

	/*
		Einfuegen von Vor- und Zurueck-Button
	*/

	vor = createBitmap("img/Graphik/vor_grau.png", 0, 0 );
	vor.x = 0.375 * screenWidth;
	vor.y = 0.85 * screenHeight;
	stage.addChild(vor);

	vor.addEventListener("click", function(event) {
		if(spieler[0][2] == ziel[0]){
			stage.removeAllChildren();
			stage.removeAllEventListeners();

			localStorage.setItem("gegangen0", spieler[0][6]);
			kuerzester = dijkstra(start[0], ziel[0], kanten, knotenanzahl);
			localStorage.setItem("kuerzester0", kuerzester);
			goToFeedback();
		}
	});

	zurueck = createBitmap("img/Graphik/zurueck.png", 0, 0 );
	zurueck.x = 0.275 * screenWidth;
	zurueck.y = 0.85 * screenHeight;
	stage.addChild(zurueck);

	zurueck.addEventListener("click", function(event) {
			stage.removeAllChildren();
			stage.removeAllEventListeners();
			MTLG.lc.goToMenu();
	});

	/*
		Wird benoetigt zum ausgrauen des vor-Pfeils, falls noch nicht alle Spieler das Ziel erreicht haben.
	*/

	var vorBit = new Array(2);
	vorBit[0] = new createjs.Bitmap("img/Graphik/vor.png");
	vorBit[1] = new createjs.Bitmap("img/Graphik/vor_grau.png");

	createjs.Ticker.addEventListener("tick", vorTicker);
	function vorTicker(event) {
		if(!event.paused) {
			stage.update();
			if(spieler[0][2] == ziel[0]) {
				if(vor.image != vorBit[0].image){
					vor.image = vorBit[0].image;
				}
			} else {
				if(vor.image != vorBit[1].image){
					vor.image = vorBit[1].image;
				}
			stage.update();
			}
		}
	}

	/*
		Statusanzeige
	*/

	var tempx = 0.83 * screenWidth;
	var tempy = 0.3 * screenHeight;
	var rahmenText;
	rahmenText = new createjs.Shape();
	rahmenText.graphics.setStrokeStyle(4,"round").beginStroke("black").drawRect(0.83 * screenWidth, 0.3 * screenHeight, skB(450), skH(350));
	stage.addChild(rahmenText);

	var textStart = new createjs.Text("Start:" + knotenName(start[0]), "20px Arial", color(spielerFarbe));
	textStart.x = tempx + skB(5);
	textStart.y = tempy + skH(250);
	textStart.textBaseline = "alphabetic";
	stage.addChild(textStart);

	var textZiel = new createjs.Text("Ziel:" + knotenName(ziel[0]), "20px Arial", color(spielerFarbe));
	textZiel.x = tempx + skB(5);
	textZiel.y = tempy + skH(290);
	textZiel.textBaseline = "alphabetic";
	stage.addChild(textZiel);

	textWeglaenge = new createjs.Text("aktuelle Weglänge:" + 0, "20px Arial", color(spielerFarbe));
	textWeglaenge.Text = "0";
	textWeglaenge.x = tempx + skB(5);
	textWeglaenge.y = tempy + skH(330);
	textWeglaenge.textBaseline = "alphabetic";
	stage.addChild(textWeglaenge);

	var texte = new Array();
	texte[0] = "Findet den kürzesten Weg "+ knotenArtikel(ziel[0]) +".";
	texte[1] = "Die Zahlen geben an, wieviele Minuten man für den Weg braucht."
	var schritt = 0;

	frageman = createBitmap("img/Graphik/figur_frage.png", 0, 0);
	frageman.x = tempx + skB(180);
	frageman.y = tempy + skH(100);
	stage.addChild(frageman);

	sprechblase = createBitmap("img/Graphik/sprechblase_300.png",0,0);
	sprechblase.x = frageman.x - 0.1 * screenWidth;
	sprechblase.y = frageman.y - 0.25 * screenHeight;
	sprechblase.visible = false;
	stage.addChild(sprechblase);

	sprechblaseText = createText(texte[0], "black", sprechblase.x + skB(30), sprechblase.y + skH(50));
	sprechblaseText.lineWidth = 250;
	sprechblaseText.visible = false;
	stage.addChild(sprechblaseText);

	var sichtbar = false;
	frageman.addEventListener("click", function(event) {
		zaehler = 0;
		if(sichtbar == false){
			sprechblase.visible = sprechblaseText.visible = sichtbar = true;
		} else {
			sprechblase.visible = sprechblaseText.visible = sichtbar = false;
			schritt ++;
			if(schritt >= texte.length){
				schritt = 0;
			}
			sprechblaseText.text = texte[schritt];
		}
	});

	/*
		Kanten
	*/

	kanten = new Array(knotenanzahl);

	for(var i = 0; i < knotenanzahl; i++){
		kanten[i] = new Array(knotenanzahl);
	}
	if(nochmalTeam == 0){
		kanten = getKanten();
	} else {
		for(i=0; i < knotenanzahl; i++){
			for(j=0; j < knotenanzahl; j++){
				kanten[i][j] = parseInt(localStorage.getItem("kanten"+i+"_" + j));
			}
		}
	}

//--------------------------------------------------------------------------------------------------------------
//Zeichne Kanten in Spielfeld
//Durchlaufe Array kanten, um alle Kanten zu zeichnen

	var aktKanten = 0;
	linien = new Array();

	// Variablen  zum Zwischenspeichern der Mittelpunkte
	var mxa;
	var mya;
	var mxb;
	var myb;

	textKanten = new Array(kanten.length);

	kantenReihenfolge = new Array(kanten.length);
	for(i = 0; i < kanten.length; i++) {
		kantenReihenfolge[i] = new Array(kanten.length);
	}

	/*
		Zeichnen der Kanten
	*/

	for(var a = 0; a < knotenanzahl; a++){
		for(var b = a+1; b < knotenanzahl; b++){
			if(kanten[a][b] > 0){
				mxa = box[a].x - 30 + width/2;
				mya = box[a].y - 50 + height/2;
				mxb = box[b].x - 30 + width/2;
				myb = box[b].y - 50 + height/2;

				linien[aktKanten] = createLine(kantenFarbe, mxa, mya, mxb, myb);
				stage.addChild(linien[aktKanten]);

				kantenReihenfolge[a][b] = aktKanten;
				textKanten[aktKanten] = new createjs.Text(kanten[a][b], "bold 36px Arial", kantengewichtFarbe);

				if(box[a].x > box[b].x) {
					textKanten[aktKanten].x = box[b].x + Math.abs(box[a].x-box[b].x)/2;
				}
				if(box[a].x < box[b].x) {
					textKanten[aktKanten].x = box[a].x + Math.abs(box[a].x-box[b].x)/2;
				}
				if(box[a].x == box[b].x) {
					textKanten[aktKanten].x = box[a].x;
				}
				if(box[a].y > box[b].y) {
					textKanten[aktKanten].y = box[b].y + Math.abs(box[a].y-box[b].y)/2;
				}
				if(box[a].y < box[b].y) {
					textKanten[aktKanten].y = box[a].y + Math.abs(box[a].y-box[b].y)/2;
				}
				if(box[a].y == box[b].y) {
					textKanten[aktKanten].y = box[a].y + 20;
				}

				textKanten.textBaseline = "alphabetic";
				stage.addChild(textKanten[aktKanten]);
				aktKanten++;
			}
		}
	}
}

/*
	benötigt für Methodenaufruf, um die Farbe zu erhalten
*/

function color(farbe){
	switch(farbe){
		case "gruen":
			return "green";
			break;
		case "gelb":
			return "yellow";
			break;
		case "blau":
			return "blue";
			break;
		case "rot":
			return "red";
			break;
		default:
			return "white";
	}
}
