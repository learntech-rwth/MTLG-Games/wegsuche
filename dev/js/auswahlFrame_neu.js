((exports)=>{
var kastenFarbe = localStorage.getItem("kastenFarbe");
var schattenFarbe = localStorage.getItem("schattenFarbe");
var rahmenFarbe = localStorage.getItem("rahmenFarbe");
// Breite der Spielfiguren bzw. der Kaesten
var width = parseInt(localStorage.getItem("width"));
// Hoehe der Spielfiguren bzw. der Kaesten fuer die Spielfiguren
var height = parseInt(localStorage.getItem("height"));
var screenWidth;
var screenHeight;

var spielerAnzahl;
var spielModus;
var spieler = new Array(4);
var auswahlSpielmodus;
var bitmapTeam;
var bitmapGegeneinander;
var bitmapTeam_chosen;
var bitmapTeam_unchosen;
var maxAnzahlSpieler = 4;
var spielerFarbe = new Array(4);
var auswahlSpieler = new Array(maxAnzahlSpieler);
var auswahlSpieler_unchosen = new Array(maxAnzahlSpieler);
var auswahlSpieler_chosen = new Array(maxAnzahlSpieler);
var spielerFarbeBit = new Array(5);
var rot;
var blau;
var gruen;
var gelb;
var auswahlKastenFiguren;
var auswahlFigurenBilder = new Array(6);
var auswahlKastenFarbe;
var auswahlSpielfiguren;
var klick2;

var texte = new Array();

// Offset der x-Koordinaten, falls stage nicht in (0,0) liegt
var verschx;
var verschy;

/*
	preUse laedt Variablen aus dem localStorage und setzt Pfade der Bilder
*/

function preUse() {
	spielerAnzahl = parseInt(localStorage.getItem("spielerAnzahl"));
	spielModus = localStorage.getItem("spielModus");
	for(i = 0; i < 4; i++) {
		spieler[i] = parseInt(localStorage.getItem("chosenSpieler"+i));
	}
	for(i = 0; i < 4; i++) {
		spieler[i] = parseInt(localStorage.getItem("chosenSpieler"+i));
	}

	for(i = 0; i < 4; i++) {
		spielerFarbe[i] = localStorage.getItem("chosenFarbe"+i);
	}
	for(i = 0; i < auswahlSpieler_unchosen.length; i++) {
		auswahlSpieler_unchosen[i] = MTLG.assets.getBitmap("img/Figuren/Auswahlscreen/figur" + (i + 1) + ".png");
		auswahlSpieler_chosen[i] = MTLG.assets.getBitmap("img/Figuren/Auswahlscreen/figur"+ (i+1) + "_chosen.png");
	}
	spielerFarbeBit["rot"] = new Array(6);
	spielerFarbeBit["blau"] = new Array(6);
	spielerFarbeBit["gelb"] = new Array(6);
	spielerFarbeBit["gruen"] = new Array(6);
	spielerFarbeBit["blanko"] = new Array(6);

	for(i = 0; i < 6; i++) {
		spielerFarbeBit["rot"][i] = MTLG.assets.getBitmap("img/Figuren/spielfigur_"+(i+1)+"_rot.png");
		spielerFarbeBit["blau"][i] = MTLG.assets.getBitmap("img/Figuren/spielfigur_"+(i+1)+"_blau.png");
		spielerFarbeBit["gelb"][i] = MTLG.assets.getBitmap("img/Figuren/spielfigur_"+(i+1)+"_gelb.png");
		spielerFarbeBit["gruen"][i] = MTLG.assets.getBitmap("img/Figuren/spielfigur_"+(i+1)+"_gruen.png");
		spielerFarbeBit["blanko"][i] = MTLG.assets.getBitmap("img/Figuren/spielfigur_"+(i+1)+".png");
	}
	screenHeight = parseInt(localStorage.getItem("screenHeight"));
	screenWidth = parseInt(localStorage.getItem("screenWidth"));


}
/*
	andereSpielerAnzahl wird aufgerufen, wenn durch ein click-Event die SpielerAnzahl
	geaendert wurde und somit der localStorage nicht mit dem localen Wert uebereinstimmt.
	Dabei wird die Anzeige der Spielmodus-Auswahl evtl angepasst und die Anzahl der Kästen
	zur Auswahl der Spielfiguren.
*/

function andereSpielerAnzahl() {
	for(i = 0; i < 4; i++) {
		//Default Wert 2
		if(i == spielerAnzahl - 1) {
			auswahlSpieler_chosen[i].visible = true;
			auswahlSpieler_unchosen[i].visible = false;
		} else {
			auswahlSpieler_chosen[i].visible = false;
			auswahlSpieler_unchosen[i].visible = true;
		}
	}

	switch(spielerAnzahl) {
		case 1:
			auswahlSpielmodus.visible = false;
			bitmapTeam.visible = false;
			bitmapGegeneinander.visible = false;
			klick2.visible = false;
			break;
		default:
			auswahlSpielmodus.visible = true;
			bitmapTeam.visible = true;
			bitmapGegeneinander.visible = true;
			break;
	}
	for(i = 0; i < maxAnzahlSpieler; i++) {
		if((i < spielerAnzahl && spielModus == "gegeneinander") || i == 0) {
			auswahlKastenFiguren[i].visible = true;
			auswahlKastenFarbe[i].visible = true;
		} else {
			auswahlKastenFiguren[i].visible = false;
			auswahlKastenFarbe[i].visible = false;
		}
	}
}

/*
	andereSpielmodus wird aufgerufen, wenn durch ein click-Event der Spielmodus veraendert wurde.
	Dabei wird die graphische Darstellung der Spielmodusauswahl angepasst. Auch wird die Anzahl der
	Kaesten für die Spielfigurenauswahl evtl angepasst.
*/

function andereSpielmodus() {
	switch(spielModus) {
		case "team":
			bitmapTeam.image = bitmapTeam_chosen.image;
			bitmapGegeneinander.image = bitmapGegeneinander_unchosen.image;
			for(i = 0; i < maxAnzahlSpieler; i++) {
				if(i < 1) {
					auswahlKastenFiguren[i].visible = true;
					auswahlKastenFarbe[i].visible = true;
				} else {
					auswahlKastenFiguren[i].visible = false;
					auswahlKastenFarbe[i].visible = false;
				}
			}
			break;
		case "gegeneinander":
			bitmapGegeneinander.image = bitmapGegeneinander_chosen.image;
			bitmapTeam.image = bitmapTeam_unchosen.image;
			for(i = 0; i < maxAnzahlSpieler; i++) {
				if(i < spielerAnzahl) {
					auswahlKastenFiguren[i].visible = true;
					auswahlKastenFarbe[i].visible = true;
				} else {
					auswahlKastenFiguren[i].visible = false;
					auswahlKastenFarbe[i].visible = false;
				}
			}
			break;
		case "kein":
			bitmapTeam.image = bitmapTeam_unchosen.image;
			bitmapGegeneinander.image = bitmapGegeneinander_unchosen.image;
			for(i = 0; i < maxAnzahlSpieler; i++) {
				auswahlKastenFiguren[i].visible = false;
				auswahlKastenFarbe[i].visible = false;
			}
			break;
		default:
			alert("Fehler in andereSpielModus");
			break;
	}
}

/*
	setzeSpieler() sorgt fuer die richtige Anzeige der ausgewaehlten Spieler
	vorallem am Anfang um die default-Werte zu setzen.
*/

function setzeSpieler() {
	if(spielModus == "team" || spielerAnzahl == 1) {
		if(spieler[0] > -1){
			auswahlFigurenBilder[spieler[0]].x = auswahlKastenFiguren[0].x ;
			auswahlFigurenBilder[spieler[0]].y = auswahlKastenFiguren[0].y;
			auswahlFigurenBilder[spieler[0]].image = spielerFarbeBit[spielerFarbe[0]][spieler[0]].image;
			switch(spielerFarbe[0]) {
				case "rot":
					rot.x = auswahlKastenFarbe[0].x;
					rot.y = auswahlKastenFarbe[0].y;
					break;
				case "blau":
					blau.x = auswahlKastenFarbe[0].x;
					blau.y = auswahlKastenFarbe[0].y;
					break;
				case "gruen":
					gruen.x = auswahlKastenFarbe[0].x;
					gruen.y = auswahlKastenFarbe[0].y;
					break;
				case "gelb":
					gelb.x = auswahlKastenFarbe[0].x;
					gelb.y = auswahlKastenFarbe[0].y;
					break;
				case "blanko":
					break;
				default:
					alert("Defaultbereich Swítch:  Spielerfarbe[0]" + spielerFarbe[0]);
					break;
			}
		}
		for(i = 1; i < maxAnzahlSpieler; i++) {
			if(spieler[i] > -1){
				auswahlFigurenBilder[spieler[i]].x = skB(auswahlSpielfiguren.x +20 + 140 * spieler[i]);
				auswahlFigurenBilder[spieler[i]].y = skH(auswahlSpielfiguren.y + 70);
				auswahlFigurenBilder[spieler[i]].image = spielerFarbeBit["blanko"][spieler[i]].image;
				switch(spielerFarbe[i]) {
					case "rot":
						rot.x = skB(auswahlSpielfiguren.x +715);
						rot.y = skH(auswahlSpielfiguren.y +290);
						break;
					case "gruen":
						gruen.x = skB(auswahlSpielfiguren.x +715);
						gruen.y = skH(auswahlSpielfiguren.y +350);
						break;
					case "gelb":
						gelb.x = skB(auswahlSpielfiguren.x +715);
						gelb.y = skH(auswahlSpielfiguren.y +470);
						break;
					case "blau":
						blau.x = skB(auswahlSpielfiguren.x +715);
						blau.y = skH(auswahlSpielfiguren.y +410);
						break;
					case "blanko":
						break;
					default:
						alert("Defaultbereich Switch:  Spielerfarbe["+i+"]" + spielerFarbe[i]);
						break;
				}
			}
		}
	} else {
		for(i = 0; i < spielerAnzahl; i++) {
			if(spieler[i] > -1){
				auswahlFigurenBilder[spieler[i]].x = auswahlKastenFiguren[i].x;
				auswahlFigurenBilder[spieler[i]].y = auswahlKastenFiguren[i].y;
				auswahlFigurenBilder[spieler[i]].image = spielerFarbeBit[spielerFarbe[i]][spieler[i]].image;
				switch(spielerFarbe[i]) {
					case "rot":
						rot.x = auswahlKastenFarbe[i].x;
						rot.y = auswahlKastenFarbe[i].y;
						break;
					case "blau":
						blau.x = auswahlKastenFarbe[i].x;
						blau.y = auswahlKastenFarbe[i].y;
						break;
					case "gruen":
						gruen.x = auswahlKastenFarbe[i].x;
						gruen.y = auswahlKastenFarbe[i].y;
						break;
					case "gelb":
						gelb.x = auswahlKastenFarbe[i].x;
						gelb.y = auswahlKastenFarbe[i].y;
						break;
					case "blanko":
						break;
					default:
						alert("Defaultbereich Swítch:  Spielerfarbe["+i+"]" + spielerFarbe[i]);
						break;
				}
			}
		}
		for(i = spielerAnzahl; i < maxAnzahlSpieler; i++) {
			if(spieler[i] > -1){
				auswahlFigurenBilder[spieler[i]].x =  skB(auswahlSpielfiguren.x +20 + 140 * spieler[i]);
				auswahlFigurenBilder[spieler[i]].y = skH(auswahlSpielfiguren.y + 70);
				auswahlFigurenBilder[spieler[i]].image = spielerFarbeBit["blanko"][spieler[i]].image;
				switch(spielerFarbe[i]) {
					case "rot":
						rot.x = skB(auswahlSpielfiguren.x + 715);
						rot.y = skH(auswahlSpielfiguren.y + 290);
						break;
					case "gruen":
						gruen.x = skB(auswahlSpielfiguren.x + 715);
						gruen.y = skH(auswahlSpielfiguren.y + 350);
						break;
					case "gelb":
						gelb.x = skB(auswahlSpielfiguren.x + 715);
						gelb.y = skH(auswahlSpielfiguren.y + 470);
						break;
					case "blau":
						blau.x = skB(auswahlSpielfiguren.x + 715);
						blau.y = skH(auswahlSpielfiguren.y+410);
						break;
					case "blanko":
						break;
					default:
						alert("Defaultbereich Swítch:  Spielerfarbe["+i+"]" + spielerFarbe[i]);
						break;
				}
			}
		}
	}
}

/*
	inFarbrahmen wird fuer das Einrasten der Farben genutzt.
	im pressup-Event der Farbe wird es aufgerufen, um zu ueberpruefen,
	in welchen Farbrahmen die Farbe gezogen wurde.
*/

function inFarbrahmen(farbe){
	if(farbe.x >= auswahlKastenFarbe[0].x - skB(5) && farbe.x <= auswahlKastenFarbe[0].x+skB(130) && farbe.y <= auswahlKastenFarbe[0].y + skH(60) && farbe.y >= auswahlKastenFarbe[0].y - skH(30)) {
		return 0;
	}
	if(farbe.x >= auswahlKastenFarbe[1].x - skB(5) && farbe.x <= auswahlKastenFarbe[1].x+skB(130) && farbe.y <= auswahlKastenFarbe[1].y + skH(60) && farbe.y >= auswahlKastenFarbe[1].y - skH(30) ){
		return 1;
	}
	if(farbe.x >= auswahlKastenFarbe[2].x - skB(5) && farbe.x <= auswahlKastenFarbe[2].x+skB(130) && farbe.y <= auswahlKastenFarbe[2].y + skH(60) && farbe.y >= auswahlKastenFarbe[2].y - skH(30) ){
		return 2;
	}
	if(farbe.x >= auswahlKastenFarbe[3].x - skB(5) && farbe.x <= auswahlKastenFarbe[3].x+skB(130) && farbe.y <= auswahlKastenFarbe[3].y + skH(60) && farbe.y >= auswahlKastenFarbe[3].y - skH(30) ){
		return 3;
	}
	return -1;

}

/*
	inFigurrahmen funktioniert analog zu inFarbrahmen
*/

function inFigurrahmen(figur){
	if(auswahlFigurenBilder[figur].x >= auswahlKastenFiguren[0].x && auswahlFigurenBilder[figur].x <= auswahlKastenFarbe[0].x+skB(130) && auswahlFigurenBilder[figur].y <= auswahlKastenFarbe[0].y+skH(220)	&& auswahlFigurenBilder[figur].y >= auswahlKastenFiguren[0].y-skH(15)){
		return 0;
	}
	if(auswahlFigurenBilder[figur].x >= auswahlKastenFiguren[1].x && auswahlFigurenBilder[figur].x <= auswahlKastenFarbe[1].x+skB(130) && auswahlFigurenBilder[figur].y <= auswahlKastenFarbe[1].y+skH(220)	&& auswahlFigurenBilder[figur].y >= auswahlKastenFiguren[1].y-skH(15)){
		return 1;
	}
	if(auswahlFigurenBilder[figur].x >= auswahlKastenFiguren[2].x && auswahlFigurenBilder[figur].x <= auswahlKastenFarbe[2].x+skB(130) && auswahlFigurenBilder[figur].y <= auswahlKastenFarbe[2].y+skH(220)	&& auswahlFigurenBilder[figur].y >= auswahlKastenFiguren[2].y-skH(15)){
		return 2;
	}
	if(auswahlFigurenBilder[figur].x >= auswahlKastenFiguren[3].x && auswahlFigurenBilder[figur].x <= auswahlKastenFarbe[3].x+skB(130) && auswahlFigurenBilder[figur].y <= auswahlKastenFarbe[3].y+skH(220)	&& auswahlFigurenBilder[figur].y >= auswahlKastenFiguren[3].y-skH(15)){
		return 3;
	}
	return -1;
}

/*
	Die Kinder koennen zu jedem Zeitpunkt auf "Spielen" klicken
	durch checkPreconditions wird sichergestellt, dass Werte, die
	noch nicht ausgewaehlt wurden gesetzt werden.
*/

function checkPreconditions(){
	// Es wird ueberprueft, ob die Spieleranzahl gesetzt wurde, falls nicht, wird sie auf 2 gesetzt.
	if(parseInt(localStorage.getItem("spielerAnzahl")) < 1 || parseInt(localStorage.getItem("spielerAnzahl")) > 4){
		spielerAnzahl = 2;
		localStorage.setItem("spielerAnzahl", 2);
	}
	// Es wird ueberprueft, ob der SPielmodus gesetzt wurde. Falls nicht, wird er auf "team" gesetzt
	if((localStorage.getItem("spielModus") != "team" && localStorage.getItem("spielModus") != "gegeneinander")||(localStorage.getItem("spielModus") == "kein")){
		spielModus = "team";
		localStorage.setItem("spielModus", "team");
	}

	// Es wird ueberprueft, ob die Spielfiguren ausgewaehlt wurden. Falls nicht, werden die nicht ausgewaehlten auf den naechst freien Spieler (bei 0 angefangen zu zaehlen ) gesetzt.
	//zeigt auf den naechsten freien Spieler
	var naechsteFreieSpieler = 0;
	// Array, das an Stelle i = 1 ist, falls Figur (i+1) ausgewaehlt wurde und i = 0 sonst
	var gewaehlteSpieler = new Array(6);
	for(i = 0; i < 6; i++){
		if(parseInt(localStorage.getItem("chosenSpieler0")) == i || parseInt(localStorage.getItem("chosenSpieler1")) == i || parseInt(localStorage.getItem("chosenSpieler2")) == i || parseInt(localStorage.getItem("chosenSpieler3")) == i){
			gewaehlteSpieler[i] = 1;
		} else {
			gewaehlteSpieler[i] = 0;
		}
	}

	for(i = 0; i < spielerAnzahl; i++){
		while(gewaehlteSpieler[naechsteFreieSpieler] == 1){
			naechsteFreieSpieler ++;
		}
		if(parseInt(localStorage.getItem("chosenSpieler" + i)) < 0 || parseInt(localStorage.getItem("chosenSpieler" + i)) > 5){
			gewaehlteSpieler[naechsteFreieSpieler] = 1;
			spieler[i] = naechsteFreieSpieler;
			localStorage.setItem("chosenSpieler" + i, naechsteFreieSpieler);
		}
	}
	// Es wird ueberprueft, ob fuer alle Spieler eine Farbe gesetzt wurde.
	// 0 entspricht rot, 1 entspricht gruen, 2 entspricht blau, 3 entspricht gelb
	var freieFarbe = new Array(4);
	var naechsteFreieFarbe = 0;
	for(i = 0; i < 4; i ++){
		freieFarbe[i] = 0;
	}
	for( i= 0; i < maxAnzahlSpieler; i++){
		switch(localStorage.getItem("chosenFarbe"+i)){
			case "rot":
				freieFarbe[0] = 1;
				break;
			case "gruen":
				freieFarbe[1] = 1;
				break;
			case "blau":
				freieFarbe[2] = 1;
				break;
			case "gelb":
				freieFarbe[3] = 1;
				break;
		}
	}
	for(i = 0; i < maxAnzahlSpieler; i++){
		while(freieFarbe[naechsteFreieFarbe] == 1) {
			naechsteFreieFarbe++;
		}
		if(localStorage.getItem("chosenFarbe" + i) != "rot" && localStorage.getItem("chosenFarbe" + i) != "blau" && localStorage.getItem("chosenFarbe" + i) != "gruen" && localStorage.getItem("chosenFarbe" + i) != "gelb"){
			switch(naechsteFreieFarbe){
				case 0:
					localStorage.setItem("chosenFarbe"+i, "rot");
					spielerFarbe[i] = "rot";
					break;
				case 1:
					localStorage.setItem("chosenFarbe"+i, "gruen");
					spielerFarbe[i] = "gruen";
					break;
				case 2:
					localStorage.setItem("chosenFarbe"+i, "blau");
					spielerFarbe[i] = "blau";
					break;
				case 3:
					localStorage.setItem("chosenFarbe"+i, "gelb");
					spielerFarbe[i] = "gelb";
					break;
				default:
					break;
			}
			freieFarbe[naechsteFreieFarbe] = 1;
		}
	}
}

/*
	Funktion, die von auswahl.html aus aufgerufen wird
*/

exports.auswahlFrame = function auswahlFrame() {
	preUse();
	verschx = stage.x;
	verschy = stage.y;

	/*
		Neustartbutton bei Inaktivitaet
	*/

	var zaehler = 0;

	var hintergrund = createFrame("","white",0,0, screenWidth, screenHeight);
	hintergrund.alpha = 0.5;
	var neustartButton = createFrame(localStorage.getItem("rahmenFarbe"), "brown", 0.45 * screenWidth, 0.45 * screenHeight, 300, 150);
	var neustartText = createText("Neustart", "black", neustartButton.x + skB(100), neustartButton.y + skH(50));

	hintergrund.on("click", function(event){
		stage.removeChild(neustartButton, neustartText, hintergrund);
		zaehler = 0;
	});

	neustartButton.addEventListener("pressup", function(event) {
		resetGame();
	});

	createjs.Ticker.addEventListener("tick", neustartTicker);
	function neustartTicker(event) {
		if(zaehler < localStorage.getItem("timeout") ){
			zaehler++;
		} else {
			stage.addChild(hintergrund);
			stage.addChild(neustartButton);
			stage.addChild(neustartText);
		}
	}

	/*
		Auswahlfeld Spieleranzahl
	*/

	var auswahlSpieleranzahl = new createjs.Shape();
	auswahlSpieleranzahl.graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,skB(800),skH(400), 10);
	auswahlSpieleranzahl.x = skB(30)+verschx;
	auswahlSpieleranzahl.y = skH(40)+verschy;

	stage.addChild(auswahlSpieleranzahl);

	/*
		Die auszuwählenden Bitmaps für die Anzahl der Spieler
	*/

	for(i = 0; i < 4; i++) {
		//Default Wert 2
		if(i == spielerAnzahl - 1) {
			auswahlSpieler_chosen[i].visible = true;
			auswahlSpieler_unchosen[i].visible = false;
		} else {
			auswahlSpieler_chosen[i].visible = false;
			auswahlSpieler_unchosen[i].visible = true;
		}
		auswahlSpieler_chosen[i].x = (auswahlSpieler_unchosen[i].x = skB(80 + 200 * i) + verschx);
		auswahlSpieler_chosen[i].y = (auswahlSpieler_unchosen[i].y = skH(140) + verschy);
		stage.addChild(auswahlSpieler_chosen[i]);
		stage.addChild(auswahlSpieler_unchosen[i]);
	}

	auswahlSpieler_unchosen[0].on("click", function(event) {
		spielerAnzahl = 1;
	});

	auswahlSpieler_unchosen[1].on("click", function(event) {
		spielerAnzahl = 2;
	});

	auswahlSpieler_unchosen[2].on("click", function(event) {
		spielerAnzahl = 3;
	});

	auswahlSpieler_unchosen[3].on("click", function(event) {
		spielerAnzahl = 4;
	});

	/*
		Auswahlfeld Spielmodus
	*/

	auswahlSpielmodus = new createjs.Shape();
	auswahlSpielmodus.graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,skB(800),skH(400), 10);
	auswahlSpielmodus.x = skB(930) + verschx;
	auswahlSpielmodus.y = skH(40) + verschy;
	stage.addChild(auswahlSpielmodus);

	bitmapTeam_unchosen = MTLG.assets.getBitmap("img/Figuren/Modi/team.png");
	bitmapTeam_chosen = MTLG.assets.getBitmap("img/Figuren/Modi/team_chosen.png");
	bitmapGegeneinander_unchosen = MTLG.assets.getBitmap("img/Figuren/Modi/gegeneinander.png");
	bitmapGegeneinander_chosen = MTLG.assets.getBitmap("img/Figuren/Modi/gegeneinander_chosen.png");

	bitmapTeam = new createjs.Bitmap(bitmapTeam_unchosen.image);
	bitmapTeam.x = skB(965) + verschx;
	bitmapTeam.y = skH(100) + verschy;
	stage.addChild(bitmapTeam);
	bitmapGegeneinander = new createjs.Bitmap(bitmapGegeneinander_unchosen.image);
	bitmapGegeneinander.x = bitmapTeam.x + skB(375);
	bitmapGegeneinander.y = bitmapTeam.y;
	stage.addChild(bitmapGegeneinander);

	switch(spielModus) {
		case "team":
			bitmapTeam.image = bitmapTeam_chosen.image;
			bitmapGegeneinander.image = bitmapGegeneinander_unchosen.image;
			break;
		case "gegeneinander":
			bitmapTeam.image = bitmapTeam_unchosen.image;
			bitmapGegeneinander.image = bitmapGegeneinander_chosen.image;
			break;
		case "kein":
			bitmapTeam.image = bitmapTeam_unchosen.image;
			bitmapGegeneinander.image = bitmapGegeneinander_unchosen.image;
			break;
		default:
			bitmapTeam.image = bitmapTeam_unchosen.image;
			bitmapGegeneinander.image = bitmapGegeneinander_unchosen.image;
			break;
	}

	bitmapTeam.on("click", function(event) {
		spielModus = "team";
	});

	bitmapGegeneinander.on("click", function(event) {
		spielModus = "gegeneinander";
	});

	/*
		Auswahlfeld fuer Spielfiguren und Farben
	*/

	auswahlSpielfiguren = new createjs.Shape();
	auswahlSpielfiguren.graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,skB(1000),skH(570), 10);
	auswahlSpielfiguren.x = skB(30) + verschx;
	auswahlSpielfiguren.y = skH(470) + verschy;
	stage.addChild(auswahlSpielfiguren);

	for(i = 0; i < auswahlFigurenBilder.length; i++) {
		auswahlFigurenBilder[i] = MTLG.assets.getBitmap("img/Figuren/Spielfiguren/spielfigur_" + (i+1) +".png");
		auswahlFigurenBilder[i].x = auswahlSpielfiguren.x +skB(20 + 140*i);
		auswahlFigurenBilder[i].y = auswahlSpielfiguren.y + skH(40);
		stage.addChild(auswahlFigurenBilder[i]);
	}

	/*
		Erstellen der Kästen um die Farbfelder reinziehen zu koennen
	*/

	auswahlKastenFarbe = new Array(maxAnzahlSpieler);

	auswahlKastenFiguren = new Array(maxAnzahlSpieler);

	for(i = 0; i < auswahlKastenFiguren.length; i++) {
		auswahlKastenFiguren[i] = new createjs.Shape();
		auswahlKastenFiguren[i].graphics.beginStroke(rahmenFarbe).drawRect(0,0,width,height);
		auswahlKastenFiguren[i].x = auswahlSpielfiguren.x +skB(20 + 140*i);
		auswahlKastenFiguren[i].y = auswahlSpielfiguren.y + skH(280);
		auswahlKastenFiguren[i].visible = false;

		stage.addChild(auswahlKastenFiguren[i]);

		auswahlKastenFarbe[i] = new createjs.Shape();
		auswahlKastenFarbe[i].graphics.beginStroke(rahmenFarbe).drawRect(0,0,width,50);
		auswahlKastenFarbe[i].x = auswahlSpielfiguren.x +skB(20 + 140*i);
		auswahlKastenFarbe[i].y = auswahlSpielfiguren.y +skH(290) +height;
		auswahlKastenFarbe[i].visible = false;
		stage.addChild(auswahlKastenFarbe[i]);
	}

	for(i = 0; i < spielerAnzahl; i++) {
		if(spielModus == "gegeneinander" || i == 0) {
			auswahlKastenFiguren[i].visible = true;
			auswahlKastenFarbe[i].visible = true;
		} else {
			auswahlKastenFiguren[i].visible = false;
			auswahlKastenFarbe[i].visible = false;
		}
	}

	/*
		Erstellen der Farbfelder
	*/

	rot = new createjs.Shape();
	rot.graphics.beginStroke("black").beginFill("red").drawRect(0,0,width, 50);
	rot.x = auswahlSpielfiguren.x + skB(715);
	rot.y = auswahlSpielfiguren.y + skH(290);
	stage.addChild(rot);

	gruen = new createjs.Shape();
	gruen.graphics.beginStroke("black").beginFill("green").drawRect(0,0,width, 50);
	gruen.x = auswahlSpielfiguren.x +skB(715);
	gruen.y = auswahlSpielfiguren.y +skH(350);
	stage.addChild(gruen);

	blau = new createjs.Shape();
	blau.graphics.beginStroke("black").beginFill("blue").drawRect(0,0,width, 50);
	blau.x = auswahlSpielfiguren.x +skB(715);
	blau.y = auswahlSpielfiguren.y +skH(410);
	stage.addChild(blau);

	gelb = new createjs.Shape();
	gelb.graphics.beginStroke("black").beginFill("yellow").drawRect(0,0,width, 50);
	gelb.x = auswahlSpielfiguren.x +skB(715);
	gelb.y = auswahlSpielfiguren.y +skH(470);
	stage.addChild(gelb);

	rot.on("pressmove", function(event) {
		zaehler = 0;
		event.target.x = event.stageX;
		event.target.y = event.stageY;
		if(rot.x <= auswahlSpielfiguren.x) {
			rot.x = auswahlSpielfiguren.x;
		}
		if(rot.x >= auswahlSpielfiguren.x+skB(873)) {
			rot.x = auswahlSpielfiguren.x+skB(873);
		}
		if(rot.y <= auswahlSpielfiguren.y) {
			rot.y = auswahlSpielfiguren.y;
		}
		if(rot.y >= auswahlSpielfiguren.y+skH(520)) {
			rot.y = auswahlSpielfiguren.y+skH(520);
		}
	});

	rot.on("pressup", function(event) {
		zaehler = 0;
		for(i = 0; i < spielerFarbe.length; i++) {
			if(spielerFarbe[i] == "rot") {
				spielerFarbe[i] = "blanko";
				auswahlFigurenBilder[spieler[i]].image = spielerFarbeBit["blanko"][spieler[i]].image;
			}
		}
		if(inFarbrahmen(rot) != -1 && spielerFarbe[inFarbrahmen(rot)] == "blanko" && spieler[inFarbrahmen(rot)] > -1) {
			spielerFarbe[inFarbrahmen(rot)] = "rot";
			auswahlFigurenBilder[spieler[inFarbrahmen(rot)]].image = spielerFarbeBit["rot"][spieler[inFarbrahmen(rot)]].image;
			rot.x = auswahlKastenFarbe[inFarbrahmen(rot)].x;
			rot.y = auswahlKastenFarbe[inFarbrahmen(rot)].y;
		} else {
			rot.x = auswahlSpielfiguren.x +skB(715);
			rot.y = auswahlSpielfiguren.y +skH(290);
		}
	});

	gruen.on("pressmove", function(event) {
		zaehler = 0;
		event.target.x = event.stageX;
		event.target.y = event.stageY;
		if(gruen.x <= auswahlSpielfiguren.x) {
			gruen.x = auswahlSpielfiguren.x;
		}
		if(gruen.x >= auswahlSpielfiguren.x+skB(873)) {
			gruen.x = auswahlSpielfiguren.x+skB(873);
		}
		if(gruen.y <= auswahlSpielfiguren.y) {
			gruen.y = auswahlSpielfiguren.y;
		}
		if(gruen.y >= auswahlSpielfiguren.y+skH(520)) {
			gruen.y = auswahlSpielfiguren.y+skH(520);
		}
	});

	gruen.on("pressup", function(event) {
		zaehler = 0;
		for(i = 0; i < spielerFarbe.length; i++) {
			if(spielerFarbe[i] == "gruen") {
				spielerFarbe[i] = "blanko";
				auswahlFigurenBilder[spieler[i]].image = spielerFarbeBit["blanko"][spieler[i]].image;
			}
		}
		if(inFarbrahmen(gruen) != -1 && spielerFarbe[inFarbrahmen(gruen)] == "blanko" && spieler[inFarbrahmen(gruen)] > -1) {
			spielerFarbe[inFarbrahmen(gruen)] = "gruen";
			auswahlFigurenBilder[spieler[inFarbrahmen(gruen)]].image = spielerFarbeBit["gruen"][spieler[inFarbrahmen(gruen)]].image;
			gruen.x = auswahlKastenFarbe[inFarbrahmen(gruen)].x;
			gruen.y = auswahlKastenFarbe[inFarbrahmen(gruen)].y;
		} else {
			gruen.x = auswahlSpielfiguren.x+skB(715);
			gruen.y = auswahlSpielfiguren.y+skH(350);
		}
	});

	blau.on("pressmove", function(event) {
		zaehler = 0;
		event.target.x = event.stageX;
		event.target.y = event.stageY;
		if(blau.x <= auswahlSpielfiguren.x) {
			blau.x = auswahlSpielfiguren.x;
		}
		if(blau.x >= auswahlSpielfiguren.x+skB(873)) {
			blau.x = auswahlSpielfiguren.x+skB(873);
		}
		if(blau.y <= auswahlSpielfiguren.y) {
			blau.y = auswahlSpielfiguren.y;
		}
		if(blau.y >= auswahlSpielfiguren.y+skH(520)) {
			blau.y = auswahlSpielfiguren.y+skH(520);
		}
	});

	blau.on("pressup", function(event) {
		zaehler = 0;
		for(i = 0; i < spielerFarbe.length; i++) {
			if(spielerFarbe[i] == "blau") {
				spielerFarbe[i] = "blanko";
				auswahlFigurenBilder[spieler[i]].image = spielerFarbeBit["blanko"][spieler[i]].image;
			}
		}
		if(inFarbrahmen(blau) != -1 && spielerFarbe[inFarbrahmen(blau)] == "blanko" && spieler[inFarbrahmen(blau)] > -1) {
			spielerFarbe[inFarbrahmen(blau)] = "blau";
			auswahlFigurenBilder[spieler[inFarbrahmen(blau)]].image = spielerFarbeBit["blau"][spieler[inFarbrahmen(blau)]].image;
			blau.x = auswahlKastenFarbe[inFarbrahmen(blau)].x;
			blau.y = auswahlKastenFarbe[inFarbrahmen(blau)].y;
		} else {
			blau.x = auswahlSpielfiguren.x+skB(715);
			blau.y = auswahlSpielfiguren.y+skH(410);
		}
	});

	gelb.on("pressmove", function(event) {
		zaehler = 0;
		event.target.x = event.stageX;
		event.target.y = event.stageY;
		if(gelb.x <= auswahlSpielfiguren.x) {
			gelb.x = auswahlSpielfiguren.x;
		}
		if(gelb.x >= auswahlSpielfiguren.x+skB(873)) {
			gelb.x = auswahlSpielfiguren.x+skB(873);
		}
		if(gelb.y <= auswahlSpielfiguren.y) {
			gelb.y = auswahlSpielfiguren.y;
		}
		if(gelb.y >= auswahlSpielfiguren.y+skH(520)) {
			gelb.y = auswahlSpielfiguren.y+skH(520);
		}
	});

	gelb.on("pressup", function(event) {
		zaehler = 0;
		for(i = 0; i < spielerFarbe.length; i++) {
			if(spielerFarbe[i] == "gelb") {
				spielerFarbe[i] = "blanko";
				auswahlFigurenBilder[spieler[i]].image = spielerFarbeBit["blanko"][spieler[i]].image;
			}
		}
		if(inFarbrahmen(gelb) != -1 && spielerFarbe[inFarbrahmen(gelb)] == "blanko" && spieler[inFarbrahmen(gelb)] > -1) {
			spielerFarbe[inFarbrahmen(gelb)] = "gelb";
			auswahlFigurenBilder[spieler[inFarbrahmen(gelb)]].image = spielerFarbeBit["gelb"][spieler[inFarbrahmen(gelb)]].image;
			gelb.x = auswahlKastenFarbe[inFarbrahmen(gelb)].x;
			gelb.y = auswahlKastenFarbe[inFarbrahmen(gelb)].y;
		} else {
			gelb.x = auswahlSpielfiguren.x+skB(715);
			gelb.y = auswahlSpielfiguren.y+skH(470);
		}
	});

	/*
		Events zum Verschieben von den Spielfiguren
	*/

	auswahlFigurenBilder[0].on("pressmove", function(event) {
		zaehler = 0;
		event.target.x = event.stageX;
		event.target.y = event.stageY;
		if(auswahlFigurenBilder[0].x <= auswahlSpielfiguren.x) {
			auswahlFigurenBilder[0].x = auswahlSpielfiguren.x;
		}
		if(auswahlFigurenBilder[0].x >= auswahlSpielfiguren.x+skB(873)) {
			auswahlFigurenBilder[0].x = auswahlSpielfiguren.x+skB(873);
		}
		if(auswahlFigurenBilder[0].y <= auswahlSpielfiguren.y) {
			auswahlFigurenBilder[0].y = auswahlSpielfiguren.y;
		}
		if(auswahlFigurenBilder[0].y >= auswahlSpielfiguren.y+skH(357)) {
			auswahlFigurenBilder[0].y = auswahlSpielfiguren.y+skH(357);
		}
	});

	auswahlFigurenBilder[0].on("pressup", function(event) {
		zaehler = 0;
		for(i = 0; i < spieler.length; i++) {
			if(spieler[i] == 0) {
				switch(spielerFarbe[i]){
					case ("rot"):
						rot.x = auswahlSpielfiguren.x+skB(715);
						rot.y = auswahlSpielfiguren.y+skH(290);
						break;
					case("gruen"):
						gruen.x = auswahlSpielfiguren.x+skB(715);
						gruen.y = auswahlSpielfiguren.y+skH(350);
						break;
					case("blau"):
						blau.x = auswahlSpielfiguren.x+skB(715);
						blau.y = auswahlSpielfiguren.y+skH(410);
						break;
					case("gelb"):
						gelb.x = auswahlSpielfiguren.x+skB(715);
						gelb.y = auswahlSpielfiguren.y+skH(470);
						break;
					case "blanko":
						break;
					default:
						alert("Defaultbereich Swítch:  Spielerfarbe["+i+"]" + spielerFarbe[i]);
						break;
				}
				spieler[i] = -1;
				auswahlFigurenBilder[0].image = spielerFarbeBit["blanko"][0].image;
				spielerFarbe[i] = "blanko";
			}
		}

		if(inFigurrahmen(0) != -1 && spieler[inFigurrahmen(0)] == "-1") {
			spieler[inFigurrahmen(0)] = 0;
			auswahlFigurenBilder[0].x = auswahlKastenFiguren[inFigurrahmen(0)].x;
			auswahlFigurenBilder[0].y = auswahlKastenFiguren[inFigurrahmen(0)].y;
		} else {
			auswahlFigurenBilder[0].x = auswahlSpielfiguren.x+skB(20);
			auswahlFigurenBilder[0].y = auswahlSpielfiguren.y+skH(40);
		}
	});

	auswahlFigurenBilder[1].on("pressmove", function(event) {
		zaehler = 0;
		event.target.x = event.stageX;
		event.target.y = event.stageY;
		if(auswahlFigurenBilder[1].x <= auswahlSpielfiguren.x) {
			auswahlFigurenBilder[1].x = auswahlSpielfiguren.x;
		}
		if(auswahlFigurenBilder[1].x >= auswahlSpielfiguren.x+skB(873)) {
			auswahlFigurenBilder[1].x = auswahlSpielfiguren.x+skB(873);
		}
		if(auswahlFigurenBilder[1].y <= auswahlSpielfiguren.y) {
			auswahlFigurenBilder[1].y = auswahlSpielfiguren.y;
		}
		if(auswahlFigurenBilder[1].y >= auswahlSpielfiguren.y+skH(357)) {
			auswahlFigurenBilder[1].y = auswahlSpielfiguren.y+skH(357);
		}
	});
	auswahlFigurenBilder[1].on("pressup", function(event) {
		zaehler = 0;
		for(i = 0; i < spieler.length; i++) {
			if(spieler[i] == 1) {
				switch(spielerFarbe[i]){
					case ("rot"):
						rot.x = auswahlSpielfiguren.x+skB(715);
						rot.y = auswahlSpielfiguren.y+skH(290);
						break;
					case("gruen"):
						gruen.x = auswahlSpielfiguren.x+skB(715);
						gruen.y = auswahlSpielfiguren.y+skH(350);
						break;
					case("blau"):
						blau.x = auswahlSpielfiguren.x+skB(715);
						blau.y = auswahlSpielfiguren.y+skH(410);
						break;
					case("gelb"):
						gelb.x = auswahlSpielfiguren.x+skB(715);
						gelb.y = auswahlSpielfiguren.y+skH(470);
						break;
					case "blanko":
						break;
					default:
						alert("Defaultbereich Swítch:  Spielerfarbe["+i+"]" + spielerFarbe[i]);
						break;
				}
				spieler[i] = -1;
				auswahlFigurenBilder[1].image = spielerFarbeBit["blanko"][1].image;
				spielerFarbe[i] = "blanko";
			}
		}

		if(inFigurrahmen(1) != -1 && spieler[inFigurrahmen(1)] == "-1") {
			spieler[inFigurrahmen(1)] = 1;
			auswahlFigurenBilder[1].x = auswahlKastenFiguren[inFigurrahmen(1)].x;
			auswahlFigurenBilder[1].y = auswahlKastenFiguren[inFigurrahmen(1)].y;
		} else {
			auswahlFigurenBilder[1].x = auswahlSpielfiguren.x+skB(160);
			auswahlFigurenBilder[1].y = auswahlSpielfiguren.y+skH(40);
		}
	});

	auswahlFigurenBilder[2].on("pressmove", function(event) {
		zaehler = 0;
		event.target.x = event.stageX;
		event.target.y = event.stageY;
		if(auswahlFigurenBilder[2].x <= auswahlSpielfiguren.x) {
			auswahlFigurenBilder[2].x = auswahlSpielfiguren.x;
		}
		if(auswahlFigurenBilder[2].x >= auswahlSpielfiguren.x+skB(873)) {
			auswahlFigurenBilder[2].x = auswahlSpielfiguren.x+skB(873);
		}
		if(auswahlFigurenBilder[2].y <= auswahlSpielfiguren.y) {
			auswahlFigurenBilder[2].y = auswahlSpielfiguren.y;
		}
		if(auswahlFigurenBilder[2].y >= auswahlSpielfiguren.y+skH(357)) {
			auswahlFigurenBilder[2].y = auswahlSpielfiguren.y+skH(357);
		}
	});

	auswahlFigurenBilder[2].on("pressup", function(event) {
		zaehler = 0;
		for(i = 0; i < spieler.length; i++) {
			if(spieler[i] == 2) {
				switch(spielerFarbe[i]){
					case ("rot"):
						rot.x = auswahlSpielfiguren.x+skB(715);
						rot.y = auswahlSpielfiguren.y+skH(290);
						break;
					case("gruen"):
						gruen.x = auswahlSpielfiguren.x+skB(715);
						gruen.y = auswahlSpielfiguren.y+skH(350);
						break;
					case("blau"):
						blau.x = auswahlSpielfiguren.x+skB(715);
						blau.y = auswahlSpielfiguren.y+skH(410);
						break;
					case("gelb"):
						gelb.x = auswahlSpielfiguren.x+skB(715);
						gelb.y = auswahlSpielfiguren.y+skH(470);
						break;
					case "blanko":
						break;
					default:
						alert("Defaultbereich Swítch:  Spielerfarbe["+i+"]" + spielerFarbe[i]);
						break;
				}
				spieler[i] = -1;
				auswahlFigurenBilder[2].image = spielerFarbeBit["blanko"][2].image;
				spielerFarbe[i] = "blanko";
			}
		}

		if(inFigurrahmen(2) != -1 && spieler[inFigurrahmen(2)] == "-1") {
			spieler[inFigurrahmen(2)] = 2;
			auswahlFigurenBilder[2].x = auswahlKastenFiguren[inFigurrahmen(2)].x;
			auswahlFigurenBilder[2].y = auswahlKastenFiguren[inFigurrahmen(2)].y;
		} else {
			auswahlFigurenBilder[2].x = auswahlSpielfiguren.x+skB(300);
			auswahlFigurenBilder[2].y = auswahlSpielfiguren.y+skH(40);
		}
	});

	auswahlFigurenBilder[3].on("pressmove", function(event) {
		zaehler = 0;
		event.target.x = event.stageX;
		event.target.y = event.stageY;
		if(auswahlFigurenBilder[3].x <= auswahlSpielfiguren.x) {
			auswahlFigurenBilder[3].x = auswahlSpielfiguren.x;
		}
		if(auswahlFigurenBilder[3].x >= auswahlSpielfiguren.x+skB(873)) {
			auswahlFigurenBilder[3].x = auswahlSpielfiguren.x+skB(873);
		}
		if(auswahlFigurenBilder[3].y <= auswahlSpielfiguren.y) {
			auswahlFigurenBilder[3].y = auswahlSpielfiguren.y;
		}
		if(auswahlFigurenBilder[3].y >= auswahlSpielfiguren.y+skH(357)) {
			auswahlFigurenBilder[3].y = auswahlSpielfiguren.y+skH(357);
		}
	});

	auswahlFigurenBilder[3].on("pressup", function(event) {
		zaehler = 0;
		for(i = 0; i < spieler.length; i++) {
			if(spieler[i] == 3) {
				switch(spielerFarbe[i]){
					case ("rot"):
						rot.x = auswahlSpielfiguren.x+skB(715);
						rot.y = auswahlSpielfiguren.y+skH(290);
						break;
					case("gruen"):
						gruen.x = auswahlSpielfiguren.x+skB(715);
						gruen.y = auswahlSpielfiguren.y+skH(350);
						break;
					case("blau"):
						blau.x = auswahlSpielfiguren.x+skB(715);
						blau.y = auswahlSpielfiguren.y+skH(410);
						break;
					case("gelb"):
						gelb.x = auswahlSpielfiguren.x+skB(715);
						gelb.y = auswahlSpielfiguren.y+skH(470);
						break;
					case "blanko":
						break;
					default:
						alert("Defaultbereich Swítch:  Spielerfarbe["+i+"]" + spielerFarbe[i]);
						break;
				}
				spieler[i] = -1;
				auswahlFigurenBilder[3].image = spielerFarbeBit["blanko"][3].image;
				spielerFarbe[i] = "blanko";
			}
		}

		if(inFigurrahmen(3) != -1 && spieler[inFigurrahmen(3)] == "-1") {
			spieler[inFigurrahmen(3)] = 3;
			auswahlFigurenBilder[3].x = auswahlKastenFiguren[inFigurrahmen(3)].x;
			auswahlFigurenBilder[3].y = auswahlKastenFiguren[inFigurrahmen(3)].y;
		} else {
			auswahlFigurenBilder[3].x = auswahlSpielfiguren.x+skB(440);
			auswahlFigurenBilder[3].y = auswahlSpielfiguren.y+skH(40);
		}
	});

	auswahlFigurenBilder[4].on("pressmove", function(event) {
		zaehler = 0;
		event.target.x = event.stageX;
		event.target.y = event.stageY;
		if(auswahlFigurenBilder[4].x <= auswahlSpielfiguren.x) {
			auswahlFigurenBilder[4].x = auswahlSpielfiguren.x;
		}
		if(auswahlFigurenBilder[4].x >= auswahlSpielfiguren.x+skB(873)) {
			auswahlFigurenBilder[4].x = auswahlSpielfiguren.x+skB(873);
		}
		if(auswahlFigurenBilder[4].y <= auswahlSpielfiguren.y) {
			auswahlFigurenBilder[4].y = auswahlSpielfiguren.y;
		}
		if(auswahlFigurenBilder[4].y >= auswahlSpielfiguren.y+skH(357)) {
			auswahlFigurenBilder[4].y = auswahlSpielfiguren.y+skH(357);
		}
	});

	auswahlFigurenBilder[4].on("pressup", function(event) {
		zaehler = 0;
		for(i = 0; i < spieler.length; i++) {
			if(spieler[i] == 4) {
				switch(spielerFarbe[i]){
					case ("rot"):
						rot.x = auswahlSpielfiguren.x+skB(715);
						rot.y = auswahlSpielfiguren.y+skH(290);
						break;
					case("gruen"):
						gruen.x = auswahlSpielfiguren.x+skB(715);
						gruen.y = auswahlSpielfiguren.y+skH(350);
						break;
					case("blau"):
						blau.x = auswahlSpielfiguren.x+skB(715);
						blau.y = auswahlSpielfiguren.y+skH(410);
						break;
					case("gelb"):
						gelb.x = auswahlSpielfiguren.x+skB(715);
						gelb.y = auswahlSpielfiguren.y+skH(470);
						break;
					case "blanko":
						break;
					default:
						alert("Defaultbereich Swítch:  Spielerfarbe["+i+"]" + spielerFarbe[i]);
						break;
				}
				spieler[i] = -1;
				auswahlFigurenBilder[4].image = spielerFarbeBit["blanko"][4].image;
				spielerFarbe[i] = "blanko";
			}
		}

		if(inFigurrahmen(4) != -1 && spieler[inFigurrahmen(4)] == "-1") {
			spieler[inFigurrahmen(4)] = 4;
			auswahlFigurenBilder[4].x = auswahlKastenFiguren[inFigurrahmen(4)].x;
			auswahlFigurenBilder[4].y = auswahlKastenFiguren[inFigurrahmen(4)].y;
		} else {
			auswahlFigurenBilder[4].x = auswahlSpielfiguren.x+skB(580);
			auswahlFigurenBilder[4].y = auswahlSpielfiguren.y+skH(40);
		}
	});

	auswahlFigurenBilder[5].on("pressmove", function(event) {
		zaehler = 0;
		event.target.x = event.stageX;
		event.target.y = event.stageY;
		if(auswahlFigurenBilder[5].x <= auswahlSpielfiguren.x) {
			auswahlFigurenBilder[5].x = auswahlSpielfiguren.x;
		}
		if(auswahlFigurenBilder[5].x >= auswahlSpielfiguren.x+skB(873)) {
			auswahlFigurenBilder[5].x = auswahlSpielfiguren.x+skB(873);
		}
		if(auswahlFigurenBilder[5].y <= auswahlSpielfiguren.y) {
			auswahlFigurenBilder[5].y = auswahlSpielfiguren.y;
		}
		if(auswahlFigurenBilder[5].y >= auswahlSpielfiguren.y+skH(357)) {
			auswahlFigurenBilder[5].y = auswahlSpielfiguren.y+skH(357);
		}
	});

	auswahlFigurenBilder[5].on("pressup", function(event) {
		zaehler = 0;
		for(i = 0; i < spieler.length; i++) {
			if(spieler[i] == 5) {
				switch(spielerFarbe[i]){
					case ("rot"):
						rot.x = auswahlSpielfiguren.x+skB(715);
						rot.y = auswahlSpielfiguren.y+skH(290);
						break;
					case("gruen"):
						gruen.x = auswahlSpielfiguren.x+skB(715);
						gruen.y = auswahlSpielfiguren.y+skH(350);
						break;
					case("blau"):
						blau.x = auswahlSpielfiguren.x+skB(715);
						blau.y = auswahlSpielfiguren.y+skH(410);
						break;
					case("gelb"):
						gelb.x = auswahlSpielfiguren.x+skB(715);
						gelb.y = auswahlSpielfiguren.y+skH(470);
						break;
					case "blanko":
						break;
					default:
						alert("Defaultbereich 5 Swítch:  Spielerfarbe["+i+"]" + spielerFarbe[i]);
						break;
				}
				spieler[i] = -1;
				auswahlFigurenBilder[5].image = spielerFarbeBit["blanko"][5].image;
				spielerFarbe[i] = "blanko";
			}
		}

		if(inFigurrahmen(5) != -1 && spieler[inFigurrahmen(5)] == "-1") {
			spieler[inFigurrahmen(5)] = 5;
			auswahlFigurenBilder[5].x = auswahlKastenFiguren[inFigurrahmen(5)].x;
			auswahlFigurenBilder[5].y = auswahlKastenFiguren[inFigurrahmen(5)].y;
		} else {
			auswahlFigurenBilder[5].x = auswahlSpielfiguren.x+skB(720);
			auswahlFigurenBilder[5].y = auswahlSpielfiguren.y+skH(40);
		}
	});

	setzeSpieler();

	/*
		Vor-Button
	*/

	var vor = createBitmap("img/Graphik/vor.png", 0, 0 );
	vor.x = 0.7 * screenWidth + verschx;
	vor.y = 0.85 * screenHeight + verschy;
	stage.addChild(vor);

	vor.addEventListener("click", function(event) {
		masterTicker.paused = true;
		//Die Variablen für das Spielfeld setzen
		localStorage.setItem("spielModus", spielModus);
		for(i = 0; i < spielerFarbe.length; i++) {
			localStorage.setItem("chosenSpieler" + i, spieler[i]);
			localStorage.setItem("chosenFarbe"+i, spielerFarbe[i]);
		}
		checkPreconditions();
		MTLG.lc.goToLogin();
	});

	var zurueck = createBitmap("img/Graphik/zurueck.png", 0, 0 );
	zurueck.x = 0.6 * screenWidth + verschx;
	zurueck.y = 0.85 * screenHeight + verschy;
	stage.addChild(zurueck);

	zurueck.addEventListener("click", function(event) {
			stage.removeAllChildren();
			stage.removeAllEventListeners();
			resetGame();
	});

	/*
		Frage-Männchen
	*/

	texte[0] = "Wenn ihr Fragen habt, klickt auf mich.";
	texte[1] = "Wie viele Spieler seid ihr?";
	texte[2] = "Wollt ihr zusammen oder gegeneinander spielen?";
	texte[3] = " Sucht euch eine Spielfigur und eine Farbe aus.";
	texte[4] = " Klickt auf den grünen Pfeil. Dann geht es los.";

	var frageman = createBitmap("img/Graphik/figur_frage.png", 0, 0);
	frageman.x = 0.7 * screenWidth + verschx;
	frageman.y = 0.65 * screenHeight + verschy;
	stage.addChild(frageman);

	var sprechblase = createBitmap("img/Graphik/sprechblase_300.png",0,0);
	sprechblase.x = 0.58 * screenWidth + verschx;
	sprechblase.y = 0.4 * screenHeight + verschy;
	stage.addChild(sprechblase);

	var sprechblaseText = createText(texte[0], "black", sprechblase.x + 45, sprechblase.y + 50);
	sprechblaseText.lineWidth = 250;
	stage.addChild(sprechblaseText);

	var sichtbar = true;
	var schritt = 0;

	frageman.addEventListener("click", function(event) {
		if(sichtbar == false){
			sprechblase.visible = sprechblaseText.visible = sichtbar = true;
		} else {
			sprechblase.visible = sprechblaseText.visible = sichtbar = false;
		}
		if(schritt == 0){
			schritt = 1;
		}
	});

	function setzeText(schritt){
		sprechblaseText.text = texte[schritt];
	}

	/*
		Einfuegen der Klick-Symbole
	*/

	var klick1 = MTLG.assets.getBitmap("img/Graphik/click.png");
	klick1.x = auswahlSpieleranzahl.x + skB(5);
	klick1.y = auswahlSpieleranzahl.y + skH(5);
	stage.addChild(klick1);
	klick2 = MTLG.assets.getBitmap("img/Graphik/click.png");
	klick2.x = auswahlSpielmodus.x + skB(5);
	klick2.y = auswahlSpielmodus.y + skH(5);
	stage.addChild(klick2);


	var ziehen = MTLG.assets.getBitmap("img/Graphik/touch.png");
	ziehen.x = auswahlSpielfiguren.x + skB(5);
	ziehen.y = auswahlSpielfiguren.y + skH(5);
	stage.addChild(ziehen);

	/*
		Nummern zur leichteren Orientierung
	*/
	var eins = createTextGroesse("1", "black", auswahlSpieleranzahl.x + skB(55), auswahlSpieleranzahl.y+skH(15),40);
	stage.addChild(eins);

	var zwei = createTextGroesse("2", "black", auswahlSpielmodus.x + skB(45), auswahlSpielmodus.y + skH(15),40);
	stage.addChild(zwei);

	var drei = createTextGroesse("3", "black", auswahlSpielfiguren.x + skB(45), auswahlSpielfiguren.y + skH(15),40);
	stage.addChild(drei);


	/*
		Master-Ticker
	*/

	createjs.Ticker.addEventListener("tick", masterTicker);
	function masterTicker(event) {
		if(!event.paused) {
			if(parseInt(localStorage.getItem("spielerAnzahl")) != spielerAnzahl) {
				setzeSpieler();
				localStorage.setItem("spielerAnzahl", spielerAnzahl);
				andereSpielerAnzahl();
			}
			if(localStorage.getItem("spielModus") != spielModus) {
				setzeSpieler();
				localStorage.setItem("spielModus", spielModus);
				andereSpielmodus();
			}
			for(i = 0; i < 4; i++){
				if(localStorage.getItem("chosenSpieler"+i) != spieler[i]){
					localStorage.setItem("chosenSpieler"+i, spieler[i]);
				}
			}
			if(schritt != 0){
				schritt = 4;
				for(i= 0; i < spielerAnzahl; i++){
					if(spieler[i] == -1){
						schritt = 3;
					}
				}
				if(spielModus == "kein"){
					schritt = 2;
				}
				if(spielerAnzahl == 0){
					schritt = 1;
				}
			}
			setzeText(schritt);
			stage.update();
		}
	}
}
})(window)
