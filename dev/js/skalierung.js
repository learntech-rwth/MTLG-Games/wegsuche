/*
	Ermittelt die passende Fenstergröße für den entsprechenden Bildschirm
*/
function skalierung() {
	ratio = window.screen.width/window.screen.height;

	if(ratio == (16/9)) {
		localStorage.setItem("screenWidth", window.screen.width);
		localStorage.setItem("screenHeight", window.screen.height);
		stage.x = 0;
		stage.y = 0;
	} else if (ratio > (16/9)) {
		localStorage.setItem("screenHeight", window.screen.height);
		localStorage.setItem("screenWidth", window.screen.height * (16/9));
		stage.y = 0;
		stage.x = (window.screen.width - parseInt(localStorage.getItem("screenWidth")))/2;
	} else {
		localStorage.setItem("screenWidth", window.screen.width);
		localStorage.setItem("screenHeight", window.screen.width * (9/16));
		stage.x = 0;
		stage.y = (window.screen.width - parseInt(localStorage.getItem("screenHeight")))/2;
	}

	return stage;
}
