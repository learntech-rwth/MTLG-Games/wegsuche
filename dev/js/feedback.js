/*
	Variablen aus dem localStorage
*/

var kastenFarbe;
var rahmenFarbe;
var spielerAnzahl;
var spielModus;
var spieler;
var spielerFarbe;

var gegangen;
var kuerzester;

/*
	lokale Variablen
*/

var bewertung;
var sterne;
var spielerBit;
var vor;
var zurueck;

var screenHeight;
var screenWidth;

var buttonFarbe = "brown";

/*
	Methode, um die obigen relevanten Variablen zu initialisieren
*/

function preUse_feedback(){
	spielModus = localStorage.getItem("spielModus");
	spielerAnzahl = localStorage.getItem("spielerAnzahl");
	if(spielModus == "team"){
		spielerAnzahl = 1;
	}
	spieler = new Array(spielerAnzahl);
	spielerFarbe = new Array(spielerAnzahl);
	gegangen = new Array(spielerAnzahl);
	kuerzester = new Array(spielerAnzahl);
	for(i = 0; i < spielerAnzahl; i ++){
		spieler[i] = parseInt(localStorage.getItem("chosenSpieler" + i));
		spielerFarbe[i] = localStorage.getItem("chosenFarbe" + i);
		gegangen[i] = parseInt(localStorage.getItem("gegangen" + i));
		kuerzester[i] = parseInt(localStorage.getItem("kuerzester" + i));
	}
	kastenFarbe = localStorage.getItem("kastenFarbe");
	rahmenFarbe = localStorage.getItem("rahmenFarbe");
	screenHeight = localStorage.getItem("screenWidth");
	screenWidth = localStorage.getItem("screenHeight");
}


/*
	Aufbau des Feedback-Screens
*/

function feedback(stage) {
	preUse_feedback();
	bewertung = new Array(spielerAnzahl);
	for(i=0; i < spielerAnzahl; i++){
		bewertung[i] = rating(gegangen[i],kuerzester[i]);
	}

	var feedbackFrame = createFrame(rahmenFarbe, kastenFarbe, skB(30), skH(30), skB(930),skH(1010) );
	stage.addChild(feedbackFrame);

	sterne = new Array(spielerAnzahl);
	for(i = 0; i < spielerAnzahl; i++){
		sterne[i] = new Array(5);
		for(j = 0; j < sterne[i].length; j++) {
			if(j < bewertung[i]) {
				sterne[i][j] = createPolystar(skB(230) + feedbackFrame.x + j * skB(150), feedbackFrame.y + skH(160) + skH(250)*i, 60, 5, 0.6, -19, color(spielerFarbe[i]));
			} else {
				sterne[i][j] = createPolystar(skB(230) + feedbackFrame.x + j * skB(150), feedbackFrame.y + skH(160) + skH(250)*i, 60, 5, 0.6, -19, kastenFarbe);
			}
			stage.addChild(sterne[i][j]);
		}
	}

	spielerBit = new Array(spielerAnzahl);
	console.log(spieler);
	for(i = 0; i < spielerAnzahl; i ++){
		spielerBit[i] = createBitmap("img/Figuren/spielfigur_"+(parseInt(spieler[i])+1)+".png", skB(50), skH(80+250*i))
		stage.addChild(spielerBit[i]);
	}

	createjs.Ticker.addEventListener("tick", masterTicker);
	function masterTicker(event) {
		if(!event.paused) {
			stage.update();
		}
	}

	/*
		Einfuegen der Bilder um vor- oder zurueck zu gehen
	*/

	var vorRahmen = createFrame(rahmenFarbe, buttonFarbe, 0.95 * screenWidth, 0.05 * screenHeight, skB(600) ,skH(390));
	stage.addChild(vorRahmen);

	vor = createBitmap("img/Graphik/vor.png", 0, 0 );
	vor.x = vorRahmen.x + skB(50);
	vor.y = vorRahmen.y + skH(80);
	stage.addChild(vor);

	var vorText = new createjs.Text("Neues Spiel", "30px Arial", "black");
	vorText.x = vor.x + skB(140);
	vorText.y = vor.y - skH(30);
	vorText.textBaseline = "alphabetic";
	stage.addChild(vorText);

	var vorBit = new Array(2);
	vorBit[0] = new createBitmap("img/Graphik/auswahlscreen_team.png", 0, 0 );
	vorBit[1] = new createBitmap("img/Graphik/auswahlscreen_wett.png",0,0);
	if(spielModus == "team"){
		vor.image = vorBit[0].image;
	} else if(spielModus == "gegeneinander"){
		vor.image = vorBit[1].image;
	}

	/*
		Klicken dieses Bitmaps wieder zum Auswahlscreen gelangen
	*/

	vorRahmen.addEventListener("click", function(event) {
			stage.removeAllChildren();
			stage.removeAllEventListeners();
			localStorage.setItem("nochmal", 0);
			MTLG.lc.goToMenu();
	});

	var zurueckRahmen = createFrame(rahmenFarbe, buttonFarbe, 0.95 * screenWidth, 0.3 * screenHeight, skB(600) , skH(390));
	stage.addChild(zurueckRahmen);

	zurueck = createBitmap("img/Graphik/zurueck.png", 0, 0 );
	zurueck.x = zurueckRahmen.x + skB(50);
	zurueck.y = zurueckRahmen.y + skH(80);
	stage.addChild(zurueck);

	var zurueckText = new createjs.Text("Nochmal", "30px Arial", "black");
	zurueckText.x = zurueck.x + skB(170);
	zurueckText.y = zurueck.y - skH(30);
	zurueckText.textBaseline = "alphabetic";
	stage.addChild(zurueckText);

	/*
		Durch Klicken auf diesem Bitmap gelangt wieder zurück zum Spielfeld
	*/

	zurueckRahmen.addEventListener("click", function(event) {
		stage.removeAllChildren();
		stage.removeAllEventListeners();
		localStorage.setItem("nochmal", 1);
		MTLG.lc.levelFinished();
	});

	var zurueckBit = new Array(2);
	zurueckBit[0] = new createBitmap("img/Graphik/spielfeld_team.png", 0, 0);
	zurueckBit[1] = new createBitmap("img/Graphik/spielfeld_wett.png", 0, 0);
	if(spielModus == "team"){
		zurueck.image = zurueckBit[0].image;
	} else if(spielModus == "gegeneinander"){
		zurueck.image = zurueckBit[1].image;
	}

	/*
		Frage-Männchen
	*/

	var texte = new Array();
	texte[0] = "Herzlichen Glückwunsch!"
	texte[1] = "Ihr habt es geschafft.";
	texte[2] = "Wollt ihr euch verbessern? ";
	texte[3] = "Dann klickt auf 'Nochmal'.";
	texte[4] = "Wollt ihr neu anfangen?";
	texte[5] = "Dann klickt auf 'Neues Spiel'.";
	var schritt = 0;

	frageman = createBitmap("img/Graphik/figur_frage.png", 0, 0);
	frageman.x = zurueck.x + skB(600);
	frageman.y = zurueck.y - skH(350);
	stage.addChild(frageman);

	sprechblase = createBitmap("img/Graphik/sprechblase_300_quer.png",0,0);
	sprechblase.x = frageman.x - skB(150);
	sprechblase.y = frageman.y -skH(200);
	sprechblase.visible = false;
	stage.addChild(sprechblase);

	sprechblaseText = createText(texte[0], "black", sprechblase.x + skB(30), sprechblase.y + skH(50));
	sprechblaseText.lineWidth = 250;
	sprechblaseText.visible = false;
	stage.addChild(sprechblaseText);

	var sichtbar = false;

	frageman.addEventListener("click", function(event) {
		zaehler = 0;
		if(sichtbar == false){
			sprechblase.visible = sprechblaseText.visible = sichtbar = true;
		} else {
			sprechblase.visible = sprechblaseText.visible = sichtbar = false;
			schritt ++;
			if(schritt >= texte.length){
				schritt = 0;
			}
			sprechblaseText.text = texte[schritt];
		}
	});

	createjs.Ticker.addEventListener("tick", masterTicker);
	function masterTicker(event) {
		if(!event.paused) {
			stage.update();
		}
	}

	var zaehler = 0;

	var hintergrund = createFrame("","white",0,0, 1920, 1080);
	hintergrund.alpha = 0.5;
	hintergrund.height = screenHeight;
	hintergrund.width = screenWidth;
	var neustartButton = createFrame(localStorage.getItem("rahmenFarbe"), "brown", 0.55 * screenWidth, 0.25 * screenHeight, 300, 150);
	var neustartText = createText("Neustart", "black", neustartButton.x + 100, neustartButton.y + 50);

	hintergrund.on("click", function(event){
		stage.removeChild(neustartButton, neustartText, hintergrund);
		zaehler = 0;
	});

	neustartButton.addEventListener("pressup", function(event) {
		resetGame();
	});

	createjs.Ticker.addEventListener("tick", neustartTicker);
	function neustartTicker(event) {
		if(zaehler < localStorage.getItem("timeout") ){
			zaehler ++;
		} else {
			stage.addChild(hintergrund);
			stage.addChild(neustartButton);
			stage.addChild(neustartText);
		}
	}
}
