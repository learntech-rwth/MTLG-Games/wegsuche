/*
	Default-Werte für das AuswahlFrame
	wird am Anfang des Spiels aufgerufen
*/

function setDefaultValues() {

	/*
		Der Nutzer kann hier manuell festlegen, ob schon die Auswahl der Spielfiguren vorgegeben ist (true) oder nicht (false)
	*/

	var defaultWerte = false;

	if(defaultWerte) {
		localStorage.setItem("spielerAnzahl", 2);
		localStorage.setItem("spielModus", "team");

		localStorage.setItem("chosenSpieler0", 5);
		localStorage.setItem("chosenSpieler1", 4);
		localStorage.setItem("chosenSpieler2", 3);
		localStorage.setItem("chosenSpieler3", 2);

		localStorage.setItem("chosenFarbe0", "gruen");
		localStorage.setItem("chosenFarbe1", "blau");
		localStorage.setItem("chosenFarbe2", "gelb");
		localStorage.setItem("chosenFarbe3", "rot");

		// zum Setzen des Textes in der Sprechblase im Auswahlfeld
		localStorage.setItem("schritt", 0);
	} else {
		localStorage.setItem("spielerAnzahl", 0);
		localStorage.setItem("spielModus", "kein");

		localStorage.setItem("chosenSpieler0", -1);
		localStorage.setItem("chosenSpieler1", -1);
		localStorage.setItem("chosenSpieler2", -1);
		localStorage.setItem("chosenSpieler3", -1);

		localStorage.setItem("chosenFarbe0", "blanko");
		localStorage.setItem("chosenFarbe1", "blanko");
		localStorage.setItem("chosenFarbe2", "blanko");
		localStorage.setItem("chosenFarbe3", "blanko");
	}

	/*
		Unabhängig von der Standard Auswahl der Spielfiguren werden weitere Werte gesetzt, die für das Spielfeld relevant sind
	*/

	//counter, für Neustartbutton
	localStorage.setItem("timeout", 1000);
	//Hoehe und Breite der Spiels
	localStorage.setItem("height", 213);
	localStorage.setItem("width", 126);
	//Farben im Spielfeld
	localStorage.setItem("kastenFarbe", "#e0ab69");
	localStorage.setItem("schattenFarbe", "darkorange");
	localStorage.setItem("rahmenFarbe", "red");
	localStorage.setItem("kantengewichtFarbe", "black");
	localStorage.setItem("kantenFarbe", "#636363");
	//Variable wird genutzt um zu ueberpruefen, ob die Kanten neuberechnet werden oder aus localStorage uebernommen werden (zurueckButton vom Feedback)
	localStorage.setItem("nochmal", 0);
	localStorage.setItem("nochmalTeam", 0);
}
