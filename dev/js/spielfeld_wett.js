var kastenFarbe;
var kastenFarbe;
var schattenFarbe;
var rahmenFarbe;
var kantengewichtFarbe;
var kantenFarbe;
var spielerAnzahl;
var width;
var height;

var knotenanzahl;
var box;
var rahmen;
var start;
var ziel;
var zielFlaggen;
var spieler;
var spielerFarbe;
var bitmapSpieler;
var spielFigur;
var kanten;
var textWeglaenge;
var vor;
var zurueck;
var kantenReihenfolge;
var linien;
var nochmal;

var screenWidth;
var screenHeight;
var kastenBreite;

/*
	Methode, um die obigen Variablen zu initialisieren
*/

function preuse_spielfeldwett() {
	kastenFarbe = localStorage.getItem("kastenFarbe");
	schattenFarbe = localStorage.getItem("schattenFarbe");
	rahmenFarbe = localStorage.getItem("rahmenFarbe");
	kantengewichtFarbe = localStorage.getItem("kantengewichtFarbe");
	kantenFarbe = localStorage.getItem("kantenFarbe");
	spielerAnzahl = parseInt(localStorage.getItem("spielerAnzahl"));
	width = parseInt(localStorage.getItem("width"));
	height = parseInt(localStorage.getItem("height"));
	knotenanzahl = 29;
	nochmal = parseInt(localStorage.getItem("nochmal"));
	screenWidth = localStorage.getItem("screenWidth");
	screenHeight = localStorage.getItem("screenHeight");
	rahmenBreite =  width + 20;

	box = new Array(knotenanzahl);
	start = new Array(4);
	ziel = new Array(4);
	if(nochmal == 1){
		for(i=0; i < spielerAnzahl; i++){
			start[i] = localStorage.getItem("start" + i);
			ziel[i] = localStorage.getItem("ziel" + i);
		}
	}
	zielFlaggen = new Array(4);
	spieler = new Array(spielerAnzahl);
	spielerFarbe = new Array(spielerAnzahl);
	spielFigur = new Array(spielerAnzahl);

	for(i = 0; i < spielFigur.length; i++) {
		spielFigur[i] = localStorage.getItem("chosenSpieler"+i);
	}

	for(i = 0; i < spielerFarbe.length; i++) {
		spielerFarbe[i] = localStorage.getItem("chosenFarbe"+i);
	}

	/*
		textWeglaenge für die gegangenen Wege der Spieler
	*/

	textWeglaenge = new Array(spielerAnzahl);
}

/*
	Aufbau des entsprechenden Spielfeldes
*/

function spielfeldwett(stage) {
	preuse_spielfeldwett();
	baueSpielfeldWett(stage);

	createjs.Ticker.addEventListener("tick", handleTick);
	function handleTick(event) {
		if(!event.paused) {
			stage.update();
		}
	}

	var aktWeg = 0;

	//Array, das festhält, welche Kante schon ausgewählt wurde

	var chosenKante = new Array(spielerAnzahl);
	for(i = 0; i < spielerAnzahl; i++) {
		chosenKante[i] = new Array(knotenanzahl);
		for(j = 0; j < knotenanzahl; j++) {
			chosenKante[i][j] = new Array(knotenanzahl);
		}
	}

	for(i = 0; i < spielerAnzahl; i++) {
		for(j = 0; j < knotenanzahl; j++) {
			for(z = 0; z < knotenanzahl; z++) {
				chosenKante[i][j][z] = false;
			}
		}
 	}

	/*
		Button zum Vor und Zurueck gehen
	*/

	zurueck = createBitmap("img/Graphik/zurueck.png", 0, 0 );
	zurueck.x = 0.5 * screenWidth;
	zurueck.y = 0.88 * screenHeight;
	stage.addChild(zurueck);


	zurueck.addEventListener("click", function(event) {
			stage.removeAllChildren();
			stage.removeAllEventListeners();
			MTLG.lc.goToMenu();
	});

	vor = createBitmap("img/Graphik/vor_grau.png", 0, 0);
	vor.x = 0.6 * screenWidth;
	vor.y = zurueck.y;
	stage.addChild(vor);

	vor.addEventListener("click", function(event) {
		var kuerzester;
		if(spieler[0][2] == ziel[0] && (spielerAnzahl < 2 || spieler[1][2] == ziel[1]) && (spielerAnzahl< 3 || spieler[2][2] == ziel[2]) && (spielerAnzahl < 4 || spieler[3][2] == ziel[3])){
			stage.removeAllChildren();
			stage.removeAllEventListeners();

			for(i=0; i < spielerAnzahl; i++){
				localStorage.setItem("gegangen" + i, spieler[i][6]);
				kuerzester = dijkstra(start[i], ziel[i], kanten, knotenanzahl);
				localStorage.setItem("kuerzester" + i, kuerzester);
			}
			goToFeedback();
		}
	});

	/*
		Wird benoetigt zum ausgrauen des vor-Pfeils, falls noch nicht alle Spieler das Ziel erreicht haben.
	*/

	var vorBit = new Array(2);
	vorBit[0] = new createjs.Bitmap("img/Graphik/vor.png");
	vorBit[1] = new createjs.Bitmap("img/Graphik/vor_grau.png");

	createjs.Ticker.addEventListener("tick", vorTicker);
	function vorTicker(event) {
		if(!event.paused) {
			if(spieler[0][2] == ziel[0] && (spielerAnzahl < 2 || spieler[1][2] == ziel[1]) && (spielerAnzahl< 3 || spieler[2][2] == ziel[2]) && (spielerAnzahl < 4 || spieler[3][2] == ziel[3])) {
				if(vor.image != vorBit[0].image){
					vor.image = vorBit[0].image;
				}
			} else {
				if(vor.image != vorBit[1].image){
					vor.image = vorBit[1].image;
				}
			stage.update();
			}
		}
	}

	/*
		Frage-Männchen
	*/

	var texte = new Array();
	texte[0] = "Findet den kürzesten Weg.";
	texte[1] = "Die Zahlen geben an, wieviele Minuten man für den Weg braucht."
	texte[2] = "Wenn du dein Ziel erreicht hast, warte auf die anderen Spieler."
	var schritt = 0;


	frageman = createBitmap("img/Graphik/figur_frage.png", 0, 0);
	frageman.x = zurueck.x + skB(155);
	frageman.y = zurueck.y - skH(170);
	stage.addChild(frageman);

	sprechblase = createBitmap("img/Graphik/sprechblase_300.png",0,0);
	sprechblase.x = frageman.x - 0.1 * screenWidth;
	sprechblase.y = frageman.y - 0.2 * screenHeight;
	sprechblase.visible = false;

	sprechblaseText = createText(texte[0], "black", sprechblase.x + skB(30), sprechblase.y + skH(50));
	sprechblaseText.lineWidth = 250;
	sprechblaseText.visible = false;

	var sichtbar = false;

	frageman.addEventListener("click", function(event) {
		zaehler = 0;
		if(sichtbar == false){
			sprechblase.visible = sprechblaseText.visible = sichtbar = true;
		} else {
			sprechblase.visible = sprechblaseText.visible = sichtbar = false;
			schritt ++;
			if(schritt >= texte.length){
				schritt = 0;
			}
			sprechblaseText.text = texte[schritt];
		}
	});

	//--------------------------------------------------------------------------------------------------------------------
	//Das Spiel startet

	/*
		Das Spielerarray wird initialisiert und sein Bitmap in den Startknoten gelegt.
	*/

	spieler = new Array(spielerAnzahl);

	for(i = 0; i < spieler.length; i++) {
		spieler[i] = new Array();		//Spieler wird angelegt
		spieler[i][2] = start[i];		//aktuelle Position
		spieler[i][3] = ziel[i];		//Zielposition
		spieler[i][4] = new Array();	//speichert vom entsprechenden Spieler besuchten Knoten
		spieler[i][4][0] = start[i];	//Den Startknoten des jeweiligen Spielers initial zu den besuchten Knoten zählen
		spieler[i][5] = 1;				//Anzahl besuchter Knoten
		spieler[i][6] = 0;				//speichert die aktuelle Weglänge
		spieler[i][7] = 0;				//speichert zuletzt besuchten Knoten
	}

	var bitmapSpieler = new Array(spielerAnzahl);

	for(i = 0; i < bitmapSpieler.length; i++) {
		bitmapSpieler[i] = createBitmap("img/Figuren/spielfigur_"+(parseInt(spielFigur[i])+1)+"_"+spielerFarbe[i]+".png", box[start[i]].x -30, box[start[i]].y-50);
		bitmapSpieler[i].visible = true;
		stage.addChild(bitmapSpieler[i]);
	}

	/*
		Event-Funktion, um alle ausgewählten Spielfiguren zu bewegen
	*/

	if(spielerAnzahl > 0){
		bitmapSpieler[0].on("pressmove", function(event) {
			zaehler = 0;
			event.target.x = event.stageX;
			event.target.y = event.stageY;
		});
	}
	if(spielerAnzahl > 1){
		bitmapSpieler[1].on("pressmove", function(event) {
			zaehler = 0;
			event.target.x = event.stageX;
			event.target.y = event.stageY;
		});
	}
	if(spielerAnzahl > 2){
		bitmapSpieler[2].on("pressmove", function(event) {
			zaehler = 0;
			event.target.x = event.stageX;
			event.target.y = event.stageY;
		});
	}
	if(spielerAnzahl > 3){
		bitmapSpieler[3].on("pressmove", function(event) {
			zaehler = 0;
			event.target.x = event.stageX;
			event.target.y = event.stageY;
		});
	}

	/*
		Array, um den Vorgänger der einzelnen Spieler zu speichern
		jeweils mit -1 initialisiert
	*/

	var pre = new Array(spielerAnzahl);
	for(i = 0; i < i.length; i++) {
		pre[i] = -1;
	}

	/*
		Offset der Spieler bzgl. der boxen um komplette Ueberschneidung der Figuren zu vermeiden.
	*/

	bitmapSpieler[0].on("pressup", function(event) {
			zaehler = 0;
			var value = aufKnotenWett(bitmapSpieler[0]);
			if(value == -1 || kanten[spieler[0][2]][value] < 1) {
				bitmapSpieler[0].x = box[spieler[0][2]].x - 30;
				bitmapSpieler[0].y = box[spieler[0][2]].y - 50;
			} else{
				//Anfangsschritt
				if(pre[0] == -1 && !chosenKante[0][spieler[0][2]][value] && !chosenKante[0][value][spieler[0][2]]) {
					bitmapSpieler[0].x = box[value].x - 30;
					bitmapSpieler[0].y = box[value].y - 50;
					spieler[0][6] = spieler[0][6] + kanten[spieler[0][2]][value];
					chosenKante[0][spieler[0][2]][value] = true;
					textWeglaenge[0].text = "Weglänge:" + spieler[0][6];
					colorLine(spieler[0][2], value, color(spielerFarbe[0]));
					pre[0] = spieler[0][2];
					spieler[0][2] = value;
					spieler[0][5]++;
					spieler[0][4].push(value);
					//Wenn die Kante auf true gesetzt ist, also schonmal ausgewählt wurde
				} else if(pre[0] == value && !chosenKante[0][spieler[0][2]][value] && chosenKante[0][value][spieler[0][2]]) {
					bitmapSpieler[0].x = box[value].x - 30;
					bitmapSpieler[0].y = box[value].y - 50;
					spieler[0][6] = spieler[0][6] - kanten[spieler[0][2]][value];
					chosenKante[0][spieler[0][2]][value] = true;
					//Damit die Figur die Strecke nicht direkt zurücklaufen kann
					chosenKante[0][value][spieler[0][2]] = false;
					textWeglaenge[0].text = "Weglänge:" + spieler[0][6];
					colorLine(spieler[0][2], value, kantenFarbe);
					spieler[0][2] = value;
					spieler[0][5]--;
					pre[0] = spieler[0][4][spieler[0][5] - 2];
					spieler[0][4].pop();
				} else {
					bitmapSpieler[0].x = box[value].x - 30;
					bitmapSpieler[0].y = box[value].y - 50;
					spieler[0][6] = spieler[0][6] + kanten[spieler[0][2]][value];
					chosenKante[0][spieler[0][2]][value] = true;
					//Damit die Figur die Strecke nicht direkt zurücklaufen kann
					chosenKante[0][value][spieler[0][2]] = false;
					textWeglaenge[0].text = "Weglänge:" + spieler[0][6];
					colorLine(spieler[0][2], value, color(spielerFarbe[0]));
					pre[0] = spieler[0][2];
					spieler[0][2] = value;
					spieler[0][5]++;
					spieler[0][4].push(value);
				}
			}

	});

	if(spielerAnzahl > 1){
		bitmapSpieler[1].on("pressup", function(event) {
			zaehler = 0;
			var value = aufKnotenWett(bitmapSpieler[1]);
			if(value == -1 || kanten[spieler[1][2]][value] < 1) {
				bitmapSpieler[1].x = box[spieler[1][2]].x - 30;
				bitmapSpieler[1].y = box[spieler[1][2]].y - 50;
			} else{
				//Anfangsschritt
				if(pre[1] == -1 && !chosenKante[1][spieler[1][2]][value] && !chosenKante[1][value][spieler[1][2]]) {
					bitmapSpieler[1].x = box[value].x - 30;
					bitmapSpieler[1].y = box[value].y - 50;
					spieler[1][6] = spieler[1][6] + kanten[spieler[1][2]][value];
					chosenKante[1][spieler[1][2]][value] = true;
					textWeglaenge[1].text = "Weglänge:" + spieler[1][6];
					colorLine(spieler[1][2], value, color(spielerFarbe[1]));
					pre[1] = spieler[1][2];
					spieler[1][2] = value;
					spieler[1][5]++;
					spieler[1][4].push(value);
				//Wenn die Kante auf true gesetzt ist, also schonmal ausgewählt wurde
				} else if(pre[1] == value && !chosenKante[1][spieler[1][2]][value] && chosenKante[1][value][spieler[1][2]]) {
					bitmapSpieler[1].x = box[value].x - 30;
					bitmapSpieler[1].y = box[value].y - 50;
					spieler[1][6] = spieler[1][6] - kanten[spieler[1][2]][value];
					chosenKante[1][spieler[1][2]][value] = true;
					chosenKante[1][value][spieler[1][2]] = false; // Damit die Figur die Strecke nicht direkt zurücklaufen kann
					textWeglaenge[1].text = "Weglänge:" + spieler[1][6];
					colorLine(spieler[1][2], value, kantenFarbe);
					spieler[1][2] = value;
					spieler[1][5]--;
					pre[1] = spieler[1][4][spieler[1][5] - 2];
					spieler[1][4].pop();
				} else {
					bitmapSpieler[1].x = box[value].x - 30;
					bitmapSpieler[1].y = box[value].y - 50;
					spieler[1][6] = spieler[1][6] + kanten[spieler[1][2]][value];
					chosenKante[1][spieler[1][2]][value] = true;
					chosenKante[1][value][spieler[1][2]] = false; // Damit die Figur die Strecke nicht direkt zurücklaufen kann
					textWeglaenge[1].text = "Weglänge:" + spieler[1][6];
					colorLine(spieler[1][2], value, color(spielerFarbe[1]));
					pre[1] = spieler[1][2];
					spieler[1][2] = value;
					spieler[1][5]++;
					spieler[1][4].push(value);
				}
			}
		});
	}

	if(spielerAnzahl > 2){
		bitmapSpieler[2].on("pressup", function(event) {
			zaehler = 0;
			var value = aufKnotenWett(bitmapSpieler[2]);
			if(value == -1 || kanten[spieler[2][2]][value] < 1) {
				bitmapSpieler[2].x = box[spieler[2][2]].x - 30;
				bitmapSpieler[2].y = box[spieler[2][2]].y - 50;
			} else{
				//Anfangsschritt
				if(pre[2] == -1 && !chosenKante[2][spieler[2][2]][value] && !chosenKante[2][value][spieler[2][2]]) {
					bitmapSpieler[2].x = box[value].x - 30;
					bitmapSpieler[2].y = box[value].y - 50;
					spieler[2][6] = spieler[2][6] + kanten[spieler[2][2]][value];
					chosenKante[2][spieler[2][2]][value] = true;
					textWeglaenge[2].text = "Weglänge:" + spieler[2][6];
					colorLine(spieler[2][2], value, color(spielerFarbe[2]));
					pre[2] = spieler[2][2];
					spieler[2][2] = value;
					spieler[2][5]++;
					spieler[2][4].push(value);
				//Wenn die Kante auf true gesetzt ist, also schonmal ausgewählt wurde
				} else if(pre[2] == value && !chosenKante[2][spieler[2][2]][value] && chosenKante[2][value][spieler[2][2]]) {
					bitmapSpieler[2].x = box[value].x - 30;
					bitmapSpieler[2].y = box[value].y - 50;
					spieler[2][6] = spieler[2][6] - kanten[spieler[2][2]][value];
					chosenKante[2][spieler[2][2]][value] = true;
					//Damit die Figur die Strecke nicht direkt zurücklaufen kann
					chosenKante[2][value][spieler[2][2]] = false;
					textWeglaenge[2].text = "Weglänge:" + spieler[2][6];
					colorLine(spieler[2][2], value, kantenFarbe);
					spieler[2][2] = value;
					spieler[2][5]--;
					pre[2] = spieler[2][4][spieler[2][5] - 2];
					spieler[2][4].pop();
				} else {
					bitmapSpieler[2].x = box[value].x - 30;
					bitmapSpieler[2].y = box[value].y - 50;
					spieler[2][6] = spieler[2][6] + kanten[spieler[2][2]][value];
					chosenKante[2][spieler[2][2]][value] = true;
					//Damit die Figur die Strecke nicht direkt zurücklaufen kann
					chosenKante[2][value][spieler[2][2]] = false;
					textWeglaenge[2].text = "Weglänge:" + spieler[2][6];
					colorLine(spieler[2][2], value, color(spielerFarbe[2]));
					pre[2] = spieler[2][2];
					spieler[2][2] = value;
					spieler[2][5]++;
					spieler[2][4].push(value);
				}
			}
		});
	}

	if(spielerAnzahl > 3) {
		bitmapSpieler[3].on("pressup", function(event) {
			zaehler = 0;

			/*
				--------------------------------------------------------------------------------------------------------------------------------
				Der folgende Algorithmus berechnet die Länge der gegangenen Wege und regelt die entsprechende, farbliche Markierung der Kanten
			*/

			var value = aufKnotenWett(bitmapSpieler[3]);
			if(value == -1 || kanten[spieler[3][2]][value] < 1) {
				bitmapSpieler[3].x = box[spieler[3][2]].x - 30;
				bitmapSpieler[3].y = box[spieler[3][2]].y - 50;
			} else{
				//Anfangsschritt
				if(pre[3] == -1 && !chosenKante[3][spieler[3][2]][value] && !chosenKante[3][value][spieler[3][2]]) {
					bitmapSpieler[3].x = box[value].x - 30;
					bitmapSpieler[3].y = box[value].y - 50;
					spieler[3][6] = spieler[3][6] + kanten[spieler[3][2]][value];
					chosenKante[3][spieler[3][2]][value] = true;
					textWeglaenge[3].text = "Weglänge:" + spieler[3][6];
					colorLine(spieler[3][2], value, color(spielerFarbe[3]));
					pre[3] = spieler[3][2];
					spieler[3][2] = value;
					spieler[3][5]++;
					spieler[3][4].push(value);
				//Wenn die Kante auf true gesetzt ist, also schonmal ausgewählt wurde
				} else if(pre[3] == value && !chosenKante[3][spieler[3][2]][value] && chosenKante[3][value][spieler[3][2]]) {
					bitmapSpieler[3].x = box[value].x - 30;
					bitmapSpieler[3].y = box[value].y - 50;
					spieler[3][6] = spieler[3][6] - kanten[spieler[3][2]][value];
					chosenKante[3][spieler[3][2]][value] = true;
					chosenKante[3][value][spieler[3][2]] = false;
					//Damit die Figur die Strecke nicht direkt zurücklaufen kann
					textWeglaenge[3].text = "Weglänge:" + spieler[3][6];
					colorLine(spieler[3][2], value, kantenFarbe);
					spieler[3][2] = value;
					spieler[3][5]--;
					pre[3] = spieler[3][4][spieler[3][5] - 2];
					spieler[3][4].pop();
				} else {
					//Noch nicht ausgewählte Kante
					bitmapSpieler[3].x = box[value].x - 30;
					bitmapSpieler[3].y = box[value].y - 50;
					spieler[3][6] = spieler[3][6] + kanten[spieler[3][2]][value];
					chosenKante[3][spieler[3][2]][value] = true;
					chosenKante[3][value][spieler[3][2]] = false;
					//Damit die Figur die Strecke nicht direkt zurücklaufen kann
					textWeglaenge[3].text = "Weglänge:" + spieler[3][6];
					colorLine(spieler[3][2], value, color(spielerFarbe[3]));
					pre[3] = spieler[3][2];
					spieler[3][2] = value;
					spieler[3][5]++;
					spieler[3][4].push(value);
				}
				/*
					-------------------------------------------------------------------------------------------------------------------------------------
				*/
			}
		});
	}

	/*
		steht ganz am Ende, damit Sprechblase immer im Vordergrund ist.
	*/

	stage.addChild(sprechblase);
	stage.addChild(sprechblaseText);

	/*
		Ticker um Spielfiguren zu verschieben, falls sie auf dem gleichen Knoten liegen
	*/

	createjs.Ticker.addEventListener("tick", masterTicker);

	/*
		damit faelle nur bearbeitet werden, falls sich was geaendert hat. Da sonst rechenaufwand zu hoch wird.
		Falls spieler i und j auf gleichem Feld : glij = "false"
		Falls spieler i und j auf unterschiedlichen Feldern: glij = "true"
	*/

	var gl01 = "true";
	var gl02 = "true";
	var gl03 = "true";
	var gl12 = "true";
	var gl13 = "true";
	var gl23 = "true";
	var kuerzester = new Array(spielerAnzahl);
	var sterne = new Array(spielerAnzahl);
	var sternegezeichnet = new Array(spielerAnzahl);

	for(i = 0; i < spielerAnzahl; i++){
		kuerzester[i] = dijkstra(start[i], ziel[i], kanten, knotenanzahl);
		sterne[i] = new Array(5);
		sternegezeichnet[i] = 0;
	}

	function zeichneSterne(xPos, yPos, bewertung, i){
		for(j = 0; j < sterne[i].length; j++) {
			if(j < bewertung) {
				sterne[i][j] = createPolystarSmall( xPos + j * skB(30),yPos  + skH(10), 10, 5, 0.6, -19, color(spielerFarbe[i]));
			} else {
				sterne[i][j] = createPolystarSmall(xPos + j * skB(30), yPos + skH(10), 10, 5, 0.6, -19, "white");
			}
			stage.addChild(sterne[i][j]);
		}
	}

	function masterTicker(event) {
		if(!event.paused) {
			for(i = 0; i < spielerAnzahl; i++){
				if(spieler[i][2] == ziel[i] && sternegezeichnet[i] == 0){
					zeichneSterne( rahmen[spieler[i][2]].x + skB(15) , box[spieler[i][2]].y, rating(spieler[i][6],kuerzester[i]), i);
					sternegezeichnet[i] = 1;
				} else if(sternegezeichnet[i] == 1){
					if(spieler[i][2] != ziel[i]) {
						sternegezeichnet[i] = 0;
						for(j = 0; j < 5; j++) {
							sterne[i][j].visible = false;
							stage.removeChild(sterne[i][j]);
						}
					}
				}
			}

			// ueberpruefe zunaechst Spieleranzahl, da sonst evtl spieler[i][j] nicht definiert ist
			if(spielerAnzahl > 1) {
				//falls spieler 0 und 1 auf dem gleichen Feld stehen, werden die bitmaps verschoben
				if(spieler[0][2] == spieler[1][2] && gl01=="true") {
					bitmapSpieler[0].x = box[spieler[0][2]].x - 30-40;
					bitmapSpieler[0].y = box[spieler[0][2]].y - 50-30;
					bitmapSpieler[1].x = box[spieler[1][2]].x - 30+40;
					bitmapSpieler[1].y = box[spieler[1][2]].y - 50+30;
					//setze gl01 auf false, da die beiden Figuren auf dem gleichen Feld liegen
					gl01 = "false";
				}
				//falls einer der Spieler runtergezogen wurde, wird glij angepasst
				if(spieler[0][2] != spieler[1][2] && gl01 == "false") {
					//setze glij auf true, da die beiden Figuren wieder auf anderen Feldern liegen
					gl01 = "true";
					//wenn keine andere Bitmap mehr auf dem gleichen Bild liegt, werden die Bitmaps wieder zurueck verschoben
					if(gl02 == "true" && gl03 == "true" && gl12 == "true" && gl13 == "true") {
						bitmapSpieler[0].x = box[spieler[0][2]].x - 30;
						bitmapSpieler[0].y = box[spieler[0][2]].y - 50;
						bitmapSpieler[1].x = box[spieler[1][2]].x - 30;
						bitmapSpieler[1].y = box[spieler[1][2]].y - 50;
					}
				}
			}
			if(spielerAnzahl > 2){
				if(spieler[0][2] == spieler[2][2] && gl02=="true") {
					gl02 = "false";
					bitmapSpieler[0].x = box[spieler[0][2]].x - 30-40;
					bitmapSpieler[0].y = box[spieler[0][2]].y - 50-30;
					bitmapSpieler[2].x = box[spieler[2][2]].x - 30-40;
					bitmapSpieler[2].y = box[spieler[2][2]].y - 50+30;
				}
				if(spieler[0][2] != spieler[2][2] && gl02 == "false") {
					gl02 = "true";
					if(gl01 == "true" && gl03 == "true" && gl12 == "true" && gl23 == "true") {
						bitmapSpieler[0].x = box[spieler[0][2]].x - 30;
						bitmapSpieler[0].y = box[spieler[0][2]].y - 50;
						bitmapSpieler[2].x = box[spieler[2][2]].x - 30;
						bitmapSpieler[2].y = box[spieler[2][2]].y - 50;
					}
				}
				if(spieler[1][2] == spieler[2][2] && gl12=="true") {
					bitmapSpieler[1].x = box[spieler[1][2]].x - 30+40;
					bitmapSpieler[1].y = box[spieler[1][2]].y - 50+30;
					bitmapSpieler[2].x = box[spieler[2][2]].x - 30-40;
					bitmapSpieler[2].y = box[spieler[2][2]].y - 50+30;
					gl12 = "false";
				}
				if(spieler[1][2] != spieler[2][2] && gl12 == "false") {
					gl12 = "true";
					if( gl01 == "true" && gl13 == "true" && gl02 == "true" && gl23 == "true") {
						bitmapSpieler[1].x = box[spieler[1][2]].x - 30;
						bitmapSpieler[1].y = box[spieler[1][2]].y - 50;
						bitmapSpieler[2].x = box[spieler[2][2]].x - 30;
						bitmapSpieler[2].y = box[spieler[2][2]].y - 50;
					}
				}
			}
			if(spielerAnzahl > 3) {
				if(spieler[0][2] == spieler[3][2] && gl03=="true") {
					bitmapSpieler[0].x = box[spieler[0][2]].x - 30-40;
					bitmapSpieler[0].y = box[spieler[0][2]].y - 50-30;
					bitmapSpieler[3].x = box[spieler[3][2]].x - 30+40;
					bitmapSpieler[3].y = box[spieler[3][2]].y - 50-30;
					gl03 = "false";
				}
				if(spieler[0][2] != spieler[3][2] && gl03 == "false") {
					gl03 = "true";
					if(gl01 == "true" && gl02 == "true" && gl13 == "true" && gl23 == "true"){
						bitmapSpieler[0].x = box[spieler[0][2]].x - 30;
						bitmapSpieler[0].y = box[spieler[0][2]].y - 50;
						bitmapSpieler[3].x = box[spieler[3][2]].x - 30;
						bitmapSpieler[3].y = box[spieler[3][2]].y - 50;
					}
				}
				if(spieler[1][2] == spieler[3][2] && gl13=="true") {
					bitmapSpieler[1].x = box[spieler[1][2]].x - 30+40;
					bitmapSpieler[1].y = box[spieler[1][2]].y - 50+30;
					bitmapSpieler[3].x = box[spieler[3][2]].x - 30+40;
					bitmapSpieler[3].y = box[spieler[3][2]].y - 50-30;
					gl13 = "false";
				}
				if(spieler[1][2] != spieler[3][2] && gl13 == "false") {
					gl13 = "true";
					if(gl01 == "true" && gl12 == "true" && gl03 == "true" && gl23 == "true") {
						bitmapSpieler[1].x = box[spieler[1][2]].x - 30;
						bitmapSpieler[1].y = box[spieler[1][2]].y - 50;
						bitmapSpieler[3].x = box[spieler[3][2]].x - 30;
						bitmapSpieler[3].y = box[spieler[3][2]].y - 50;
					}
				}
				if(spieler[2][2] == spieler[3][2] && gl23 == "true") {
					bitmapSpieler[2].x = box[spieler[2][2]].x - 30-40;
					bitmapSpieler[2].y = box[spieler[2][2]].y - 50+30;
					bitmapSpieler[3].x = box[spieler[3][2]].x - 30+40;
					bitmapSpieler[3].y = box[spieler[3][2]].y - 50-30;
					gl23 = "false";
				}
				if(spieler[2][2] != spieler[3][2] && gl23 == "false") {
					gl23 = "true";
					if(gl02 == "true" && gl12 == "true" && gl03 == "true" && gl13 == "true") {
						bitmapSpieler[2].x = box[spieler[2][2]].x - 30;
						bitmapSpieler[2].y = box[spieler[2][2]].y - 50;
						bitmapSpieler[3].x = box[spieler[3][2]].x - 30;
						bitmapSpieler[3].y = box[spieler[3][2]].y - 50;
					}
				}
			}
			stage.update();
		}
	}

	function colorLine(node1, node2, lineColor) {
		var index = -1;
		if(!((kantenReihenfolge[node1][node2] == undefined) && (kantenReihenfolge[node2][node1] == undefined))) {
			if(kantenReihenfolge[node1][node2] == undefined) {
				index = kantenReihenfolge[node2][node1];
			} else {
				index = kantenReihenfolge[node1][node2];
			}
			if(lineColor == kantenFarbe){
				mxa = box[node1].x - 30 + width/2;
				mya = box[node1].y - 50 + height/2;
				mxb = box[node2].x - 30 + width/2;
				myb = box[node2].y - 50 + height/2;
				stage.removeChild(linien[index]);
			} else if(lineColor == "red") {
				mxa = box[node1].x - 30 + (width/2)+3;
				mya = box[node1].y - 50 + (height/2)+3;
				mxb = box[node2].x - 30 + (width/2)+3;
				myb = box[node2].y - 50 + (height/2)+3;
			} else if(lineColor == "green") {
				mxa = box[node1].x - 30 + (width/2)+8;
				mya = box[node1].y - 50 + (height/2)+6;
				mxb = box[node2].x - 30 + (width/2)+8;
				myb = box[node2].y - 50 + (height/2)+6;
			} else if(lineColor == "blue") {
				mxa = box[node1].x - 30 + (width/2)-3;
				mya = box[node1].y - 50 + (height/2)-3;
				mxb = box[node2].x - 30 + (width/2)-3;
				myb = box[node2].y - 50 + (height/2)-3;
			} else if(lineColor == "yellow"){
				mxa = box[node1].x - 30 + (width/2)-8;
				mya = box[node1].y - 50 + (height/2)-6;
				mxb = box[node2].x - 30 + (width/2)-8;
				myb = box[node2].y - 50 + (height/2)-6;
			}
			linien[index] = createLine(lineColor, mxa, mya, mxb, myb);
			if(lineColor == kantenFarbe){
				linien[index].alpha = 0.5;
			}
			stage.addChild(linien[index]);
		}
	}

	/*
		Neustart-button
	*/

	var zaehler = 0;

	var hintergrund = createFrame("","white",0,0, 1920, 1080);
	hintergrund.height = screenHeight;
	hintergrund.width = screenWidth;
	hintergrund.alpha = 0.5;
	var neustartButton = createFrame(localStorage.getItem("rahmenFarbe"), "brown", 0.45 * screenWidth, 0.45 * screenHeight, 300, 150);
	var neustartText = createText("Neustart", "black", neustartButton.x + 100, neustartButton.y + 50);

	hintergrund.on("click", function(event){
		stage.removeChild(neustartButton, neustartText, hintergrund);
		zaehler = 0;
	});

	neustartButton.addEventListener("pressup", function(event) {
		resetGame();
	});

	/*
		Regelt die inaktive Zeit: wenn Zähler den Timeout überschritten hat,
		wird ein Neustart-button eingeblendet
	*/

	createjs.Ticker.addEventListener("tick", neustartTicker);
	function neustartTicker(event) {
		if(zaehler <localStorage.getItem("timeout") ){
			zaehler ++;
		} else {
			stage.addChild(hintergrund);
			stage.addChild(neustartButton);
			stage.addChild(neustartText);
		}
	}

}

/*
	Funktion die überprüft, ob Bitmap auf einem knoten liegt und gibt den entsprechenden Wert zurück
*/

function aufKnotenWett(bitmap){
	for(var u = 0; u < knotenanzahl; u++) {
		if(bitmap.x >= (box[u].x - width) && bitmap.x  <= (box[u].x + width) && bitmap.y >= (box[u].y  - height) && bitmap.y <= (box[u].y + height)){
			return u;
		}
	}
	return -1;
}

/*
	Methode, die die Start- und Endknoten zufällig festlegt
*/

function setzeStartZielWett(){
	var fall = parseInt(Math.ceil(Math.random() * 4));
	switch(fall) {
		case 0:
			start[0] = 0;
			ziel[0] = 24;
			start[1] = 13;
			ziel[1] = 21;
			start[2] = 28;
			ziel[2] = 7;
			start[3] = 20;
			ziel[3] = 15;
			break;
		case 1:
			start[0] = 7;
			ziel[0] = 28;
			start[1] = 15;
			ziel[1] = 9;
			start[2] = 27;
			ziel[2] = 0;
			start[3] = 2;
			ziel[3] = 21;
			break;
		case 2:
			start[0] = 15;
			ziel[0] = 7;
			start[1] = 24;
			ziel[1] = 0;
			start[2] = 5;
			ziel[2] = 28;
			start[3] = 13;
			ziel[3] = 22;
			break;
		default:
			start[0] = 1;
			ziel[0] = 21;
			start[1] =7;
			ziel[1] =28;
			start[2] = 15;
			ziel[2] = 8;
			start[3] = 9;
			ziel[3] = 0;
			break;
	}
	for(i = 0; i < ziel.length; i++) {
		localStorage.setItem("ziel"+i, ziel[i]);
		localStorage.setItem("start"+i, start[i]);
	}
}

/*
	Methode, um die Start- und Zielknoten zu färben bzw. setzen
*/

function faerbeStartZiel(){
	for(i = 0; i < spielerAnzahl; i++){
		rahmen[29+2*i] = new createjs.Shape();
		rahmen[29+2*i].graphics.beginStroke(rahmenFarbe).beginFill(color(spielerFarbe[i])).drawRoundRect(0,0,width+20,height+10, 10);
		rahmen[29+2*i].x = rahmen[start[i]].x;
		rahmen[29+2*i].y = rahmen[start[i]].y;
		rahmen[29+2*i+1] = new createjs.Shape();
		rahmen[29+2*i+1].graphics.beginStroke(rahmenFarbe).beginFill(color(spielerFarbe[i])).drawRoundRect(0,0,width+20,height+10, 10);
		rahmen[29+2*i+1].x = rahmen[ziel[i]].x;
		rahmen[29+2*i+1].y = rahmen[ziel[i]].y;

		zielFlaggen[i] = new createjs.Bitmap("img/Graphik/zielflagge.png");
		zielFlaggen[i].x = box[ziel[i]].x-30;
		zielFlaggen[i].y = box[ziel[i]].y-50;
	}
}

function baueSpielfeldWett(stage){
	var textKanten;

	var screenHeight = localStorage.getItem("screenHeight");
	var screenWidth = localStorage.getItem("screenWidth");

	setzeStartZielWett();

	// zeigt Weglaengen an
	for(i = 0; i < spielerAnzahl; i++){
		textWeglaenge[i] = new createjs.Text("Weglänge:" + 0, "26px Arial", color(spielerFarbe[i]));
		textWeglaenge[i].x = 0.01 * screenWidth;
		textWeglaenge[i].y = 0.82 * screenHeight + i*35;
		textWeglaenge[i].textBaseline = "alphabetic";
		stage.addChild(textWeglaenge[i]);
	}

	/*
	Erzeugt Array mit 28 Knotenpunkten
	*/
	box = new Array(knotenanzahl);
	rahmen = new Array(knotenanzahl+2* spielerAnzahl);

/*
	box 0
*/
	box[0] = new createjs.Shape();
	box[0].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[0].x = 0.01796875 * screenWidth+30;
	box[0].y = 0.025 * screenHeight+50;
	box[0].setBounds(box[0].x, box[0].y, 100, 60);
	stage.addChild(box[0]);

	rahmen[0] = new createjs.Shape();
	rahmen[0].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[0].x = 0.01276042 * screenWidth;
	rahmen[0].y = 0.02037037 * screenHeight;
	stage.addChild(rahmen[0]);
/*
	box 1
*/
	box[1] = new createjs.Shape();
	box[1].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[1].x =  0.11796875 * screenWidth+30;
	box[1].y = 0.025 * screenHeight+50;
	box[1].setBounds(box[1].x, box[1].y, 100, 60);
	stage.addChild(box[1]);

	rahmen[1] = new createjs.Shape();
	rahmen[1].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[1].x = 0.11276042 * screenWidth;
	rahmen[1].y = 0.02037037 * screenHeight;
	stage.addChild(rahmen[1]);
/*
	box 2
*/
	box[2] = new createjs.Shape();
	box[2].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[2].x = 0.21796875 * screenWidth+30;
	box[2].y = 0.025 * screenHeight+50;
	box[2].setBounds(box[2].x, box[2].y, 100, 60);
	stage.addChild(box[2]);

	rahmen[2] = new createjs.Shape();
	rahmen[2].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[2].x = 0.21276042 * screenWidth;
	rahmen[2].y = 0.02037037 * screenHeight;
	stage.addChild(rahmen[2]);
/*
	box 3
*/
	box[3] = new createjs.Shape();
	box[3].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[3].x = 0.41796875 * screenWidth+30;
	box[3].y = 0.025 * screenHeight+50;
	box[3].setBounds(box[3].x, box[3].y, 100, 60);
	stage.addChild(box[3]);

	rahmen[3] = new createjs.Shape();
	rahmen[3].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[3].x = 0.41276042 * screenWidth;
	rahmen[3].y = 0.02037037 * screenHeight;
	stage.addChild(rahmen[3]);
/*
	box 4
*/
	box[4] = new createjs.Shape();
	box[4].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[4].x = 0.51796875 * screenWidth+30;
	box[4].y = 0.025 * screenHeight+50;
	box[4].setBounds(box[4].x, box[4].y, 100, 60);
	stage.addChild(box[4]);

	rahmen[4] = new createjs.Shape();
	rahmen[4].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[4].x = 0.51276042 * screenWidth;
	rahmen[4].y = 0.02037037 * screenHeight;
	stage.addChild(rahmen[4]);
/*
	box 5
*/
	box[5] = new createjs.Shape();
	box[5].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30,-50, width, height);
	box[5].x = 0.71796875 * screenWidth+30;
	box[5].y = 0.025 * screenHeight+50;
	box[5].setBounds(box[5].x, box[5].y, 100, 60);
	stage.addChild(box[5]);

	rahmen[5] = new createjs.Shape();
	rahmen[5].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[5].x = 0.71276042 * screenWidth;
	rahmen[5].y = 0.02037037 * screenHeight;
	stage.addChild(rahmen[5]);
/*
	box 6
*/
	box[6] = new createjs.Shape();
	box[6].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[6].x = 0.81796875 * screenWidth+30;
	box[6].y = 0.025 * screenHeight+50;
	box[6].setBounds(box[6].x, box[6].y, 100, 60);
	stage.addChild(box[6]);

	rahmen[6] = new createjs.Shape();
	rahmen[6].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[6].x = 0.81276042 * screenWidth;
	rahmen[6].y = 0.02037037 * screenHeight;
	stage.addChild(rahmen[6]);
/*
	box 7
*/
	box[7] = new createjs.Shape();
	box[7].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[7].x = 0.91796875 * screenWidth+30;
	box[7].y = 0.025 * screenHeight+50;
	box[7].setBounds(box[7].x, box[7].y, 100, 60);
	stage.addChild(box[7]);

	rahmen[7] = new createjs.Shape();
	rahmen[7].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[7].x = 0.91276042 * screenWidth;
	rahmen[7].y = 0.02037037 * screenHeight;
	stage.addChild(rahmen[7]);
/*
	box 8
*/
	box[8] = new createjs.Shape();
	box[8].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[8].x = 0.91796875 * screenWidth+30;
	box[8].y = 0.275 * screenHeight+50;
	box[8].setBounds(box[8].x, box[8].y, 100, 60);
	stage.addChild(box[8]);

	rahmen[8] = new createjs.Shape();
	rahmen[8].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[8].x = 0.91276042 * screenWidth;
	rahmen[8].y = 0.27037037 * screenHeight;
	stage.addChild(rahmen[8]);
/*
	box 9
*/
	box[9] = new createjs.Shape();
	box[9].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[9].x = 0.71796875 * screenWidth+30;
	box[9].y = 0.275 * screenHeight+50;
	box[9].setBounds(box[9].x, box[9].y, 100, 60);
	stage.addChild(box[9]);

	rahmen[9] = new createjs.Shape();
	rahmen[9].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[9].x = 0.71276042 * screenWidth;
	rahmen[9].y = 0.27037037 * screenHeight;
	stage.addChild(rahmen[9]);
/*
	box 10
*/
	box[10] = new createjs.Shape();
	box[10].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[10].x = 0.61796875 * screenWidth+30;
	box[10].y = 0.275 * screenHeight+50;
	box[10].setBounds(box[10].x, box[10].y, 100, 60);
	stage.addChild(box[10]);

	rahmen[10] = new createjs.Shape();
	rahmen[10].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[10].x = 0.61276042 * screenWidth;
	rahmen[10].y = 0.27037037 * screenHeight;
	stage.addChild(rahmen[10]);
/*
	box 11
*/
	box[11] = new createjs.Shape();
	box[11].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[11].x = 0.41796875 * screenWidth+30;
	box[11].y = 0.275 * screenHeight+50;
	box[11].setBounds(box[11].x, box[11].y, 100, 60);
	stage.addChild(box[11]);

	rahmen[11] = new createjs.Shape();
	rahmen[11].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[11].x = 0.41276042 * screenWidth;
	rahmen[11].y = 0.27037037 * screenHeight;
	stage.addChild(rahmen[11]);
/*
	box 12
*/
	box[12] = new createjs.Shape();
	box[12].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[12].x = 0.31796875 * screenWidth+30;
	box[12].y = 0.275 * screenHeight+50;
	box[12].setBounds(box[12].x, box[12].y, 100, 60);
	stage.addChild(box[12]);

	rahmen[12] = new createjs.Shape();
	rahmen[12].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[12].x = 0.31276042 * screenWidth;
	rahmen[12].y = 0.27037037 * screenHeight;
	stage.addChild(rahmen[12]);
/*
	box 13
*/
	box[13] = new createjs.Shape();
	box[13].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[13].x = 0.21796875 * screenWidth+30;
	box[13].y = 0.275 * screenHeight+50;
	box[13].setBounds(box[13].x, box[13].y, 100, 60);
	stage.addChild(box[13]);

	rahmen[13] = new createjs.Shape();
	rahmen[13].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[13].x = 0.21276042 * screenWidth;
	rahmen[13].y = 0.27037037 * screenHeight;
	stage.addChild(rahmen[13]);
/*
	box 14
*/
	box[14] = new createjs.Shape();
	box[14].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[14].x = 0.01796875 * screenWidth+30;
	box[14].y = 0.275 * screenHeight+50;
	box[14].setBounds(box[14].x, box[14].y, 100, 60);
	stage.addChild(box[14]);

	rahmen[14] = new createjs.Shape();
	rahmen[14].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[14].x = 0.01276042 * screenWidth;
	rahmen[14].y = 0.27037037 * screenHeight;
	stage.addChild(rahmen[14]);
/*
	box 15
*/
	box[15] = new createjs.Shape();
	box[15].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[15].x = 0.01796875 * screenWidth+30;
	box[15].y = 0.525 * screenHeight+50;
	box[15].setBounds(box[15].x, box[15].y, 100, 60);
	stage.addChild(box[15]);

	rahmen[15] = new createjs.Shape();
	rahmen[15].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[15].x = 0.01276042 * screenWidth;
	rahmen[15].y = 0.52037037 * screenHeight;
	stage.addChild(rahmen[15]);
/*
	box 16
*/
	box[16] = new createjs.Shape();
	box[16].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[16].x = 0.11796875 * screenWidth+30;
	box[16].y = 0.525 * screenHeight+50;
	box[16].setBounds(box[16].x, box[16].y, 100, 60);
	stage.addChild(box[16]);

	rahmen[16] = new createjs.Shape();
	rahmen[16].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0, 0, rahmenBreite,height+10, 10);
	rahmen[16].x = 0.11276042 * screenWidth;
	rahmen[16].y = 0.52037037 * screenHeight;
	stage.addChild(rahmen[16]);
/*
	box 17
*/
	box[17] = new createjs.Shape();
	box[17].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[17].x = 0.21796875 * screenWidth+30;
	box[17].y = 0.525 * screenHeight+50;
	box[17].setBounds(box[17].x, box[17].y, 100, 60);
	stage.addChild(box[17]);

	rahmen[17] = new createjs.Shape();
	rahmen[17].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[17].x = 0.21276042 * screenWidth;
	rahmen[17].y = 0.52037037 * screenHeight;
	stage.addChild(rahmen[17]);
/*
	box 18
*/
	box[18] = new createjs.Shape();
	box[18].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[18].x = 0.51796875 * screenWidth+30;
	box[18].y = 0.525 * screenHeight+50;
	box[18].setBounds(box[18].x, box[18].y, 100, 60);
	stage.addChild(box[18]);

	rahmen[18] = new createjs.Shape();
	rahmen[18].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[18].x = 0.51276042 * screenWidth;
	rahmen[18].y = 0.52037037 * screenHeight;
	stage.addChild(rahmen[18]);
/*
	box 19
*/
	box[19] = new createjs.Shape();
	box[19].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[19].x = 0.61796875 * screenWidth+30;
	box[19].y = 0.525 * screenHeight+50;
	box[19].setBounds(box[19].x, box[19].y, 100, 60);
	stage.addChild(box[19]);

	rahmen[19] = new createjs.Shape();
	rahmen[19].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[19].x = 0.61276042 * screenWidth;
	rahmen[19].y = 0.52037037 * screenHeight;
	stage.addChild(rahmen[19]);
/*
	box 20
*/
	box[20] = new createjs.Shape();
	box[20].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[20].x = 0.81796875 * screenWidth+30;
	box[20].y = 0.525 * screenHeight+50;
	box[20].setBounds(box[20].x, box[20].y, 100, 60);
	stage.addChild(box[20]);

	rahmen[20] = new createjs.Shape();
	rahmen[20].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[20].x = 0.81276042 * screenWidth;
	rahmen[20].y = 0.52037037 * screenHeight;
	stage.addChild(rahmen[20]);
/*
	box 21
*/
	box[21] = new createjs.Shape();
	box[21].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[21].x = 0.91796875 * screenWidth+30;
	box[21].y = 0.525 * screenHeight+50;
	box[21].setBounds(box[21].x, box[21].y, 100, 60);
	stage.addChild(box[21]);

	rahmen[21] = new createjs.Shape();
	rahmen[21].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[21].x = 0.91276042 * screenWidth;
	rahmen[21].y = 0.52037037 * screenHeight;
	stage.addChild(rahmen[21]);
/*
	box 22
*/
	box[22] = new createjs.Shape();
	box[22].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[22].x = 0.91796875 * screenWidth+30;
	box[22].y = 0.775 * screenHeight+50;
	box[22].setBounds(box[22].x, box[22].y, 100, 60);
	stage.addChild(box[22]);

	rahmen[22] = new createjs.Shape();
	rahmen[22].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[22].x = 0.91276042 * screenWidth;
	rahmen[22].y = 0.77037037 * screenHeight;
	stage.addChild(rahmen[22]);
/*
	box 22
*/
	box[22] = new createjs.Shape();
	box[22].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[22].x = 0.91796875 * screenWidth+30;
	box[22].y = 0.775 * screenHeight+50;
 	box[22].setBounds(box[22].x, box[22].y, 100, 60);
	stage.addChild(box[22]);

	rahmen[22] = new createjs.Shape();
	rahmen[22].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[22].x = 0.91276042 * screenWidth;
	rahmen[22].y = 0.77037037 * screenHeight;
	stage.addChild(rahmen[22]);
/*
	box 23
*/
	box[23] = new createjs.Shape();
	box[23].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[23].x = 0.81796875 * screenWidth+30;
	box[23].y = 0.775 * screenHeight+50;
	box[23].setBounds(box[23].x, box[23].y, 100, 60);
	stage.addChild(box[23]);

	rahmen[23] = new createjs.Shape();
	rahmen[23].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[23].x = 0.81276042 * screenWidth;
	rahmen[23].y = 0.77037037 * screenHeight;
	stage.addChild(rahmen[23]);
/*
	box 24
*/
	box[24] = new createjs.Shape();
	box[24].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[24].x = 0.71796875 * screenWidth+30;
	box[24].y = 0.775 * screenHeight+50;
	box[24].setBounds(box[24].x, box[24].y, 100, 60);
	stage.addChild(box[24]);

	rahmen[24] = new createjs.Shape();
	rahmen[24].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0, 0,rahmenBreite,height+10, 10);
	rahmen[24].x = 0.71276042 * screenWidth;
	rahmen[24].y = 0.77037037 * screenHeight;
	stage.addChild(rahmen[24]);
/*
	box 25
*/
	box[25] = new createjs.Shape();
	box[25].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[25].x = 0.41796875 * screenWidth+30;
	box[25].y = 0.775 * screenHeight+50;
	box[25].setBounds(box[25].x, box[25].y, 100, 60);
	stage.addChild(box[25]);

	rahmen[25] = new createjs.Shape();
	rahmen[25].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[25].x = 0.41276042 * screenWidth;
	rahmen[25].y = 0.77037037 * screenHeight;
	stage.addChild(rahmen[25]);
/*
	box 26
*/
	box[26] = new createjs.Shape();
	box[26].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[26].x = 0.31796875 * screenWidth+30;
	box[26].y = 0.775 * screenHeight+50;
	box[26].setBounds(box[26].x, box[26].y, 100, 60);
	stage.addChild(box[26]);

	rahmen[26] = new createjs.Shape();
	rahmen[26].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[26].x = 0.31276042 * screenWidth;
	rahmen[26].y = 0.77037037 * screenHeight;
	stage.addChild(rahmen[26]);
/*
	box 27
*/
	box[27] = new createjs.Shape();
	box[27].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[27].x = 0.21796875 * screenWidth+30;
	box[27].y = 0.775 * screenHeight+50;
	box[27].setBounds(box[27].x, box[27].y, 100, 60);
	stage.addChild(box[27]);

	rahmen[27] = new createjs.Shape();
	rahmen[27].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[27].x = 0.21276042 * screenWidth;
	rahmen[27].y = 0.77037037 * screenHeight;
	stage.addChild(rahmen[27]);
/*
	box 28
*/
	box[28] = new createjs.Shape();
	box[28].graphics.setStrokeStyle(4,"round").beginStroke(schattenFarbe).drawRect(-30, -50, width, height);
	box[28].x = 0.11796875 * screenWidth+30;
	box[28].y = 0.775 * screenHeight+50;
	box[28].setBounds(box[28].x, box[28].y, 100, 60);
	stage.addChild(box[28]);

	rahmen[28] = new createjs.Shape();
	rahmen[28].graphics.beginStroke(rahmenFarbe).beginFill(kastenFarbe).drawRoundRect(0,0,rahmenBreite,height+10, 10);
	rahmen[28].x = 0.11276042 * screenWidth;
	rahmen[28].y = 0.77037037 * screenHeight;
	stage.addChild(rahmen[28]);

	faerbeStartZiel();
	for(i = 0; i < spielerAnzahl; i++){
		stage.addChild(rahmen[29+2*i]);
		stage.addChild(rahmen[29+2*i+1]);
		stage.addChild(zielFlaggen[i]);
	}

	/*
		Kanten
	*/

	kanten = new Array(knotenanzahl);
	for(var i = 0; i < knotenanzahl; i++){
		kanten[i] = new Array(knotenanzahl);
	}
	if(nochmal == 0){
		kanten = getKantenWett();
	} else {
		for(i=0; i < knotenanzahl; i++){
			for(j=0; j < knotenanzahl; j++){
				kanten[i][j] = parseInt(localStorage.getItem("kanten"+i+"_" + j));
			}
		}
	}

//--------------------------------------------------------------------------------------------------------------
//Zeichne Kanten in Spielfeld
//Durchlaufe Array kanten, um alle Kanten zu zeichnen

	var aktKanten = 0;
	linien = new Array();

	//Variablen  zum Zwischenspeichern der Mittelpunkte
	var mxa;
	var mya;
	var mxb;
	var myb;

	textKanten = new Array(kanten.length);

	kantenReihenfolge = new Array(kanten.length);
	for(i = 0; i < kanten.length; i++) {
		kantenReihenfolge[i] = new Array(kanten.length);
	}

	for(var a = 0; a < knotenanzahl; a++){
		for(var b = a + 1; b < knotenanzahl; b++) {
			if(kanten[a][b] > 0) {
				mxa = box[a].x -30  + width/2;

				mya = box[a].y -50 + height/2;

				mxb = box[b].x -30 + width/2;

				myb = box[b].y -50 + height/2;

				linien[aktKanten] = createLine(kantenFarbe, mxa, mya, mxb, myb);
				linien[aktKanten].alpha = 0.5;

				stage.addChild(linien[aktKanten]);

				kantenReihenfolge[a][b] = aktKanten;

				textKanten[aktKanten] = new createjs.Text(kanten[a][b], "bold 36px Arial", kantengewichtFarbe);

				if(box[a].x > box[b].x) {
					textKanten[aktKanten].x = box[b].x + Math.abs(box[a].x-box[b].x)/2;
				}
				if(box[a].x < box[b].x) {
					textKanten[aktKanten].x = box[a].x + Math.abs(box[a].x-box[b].x)/2;
				}
				if(box[a].x == box[b].x) {
					textKanten[aktKanten].x = box[a].x;
				}
				if(box[a].y > box[b].y) {
					textKanten[aktKanten].y = box[b].y + Math.abs(box[a].y-box[b].y)/2;
				}
				if(box[a].y < box[b].y) {
					textKanten[aktKanten].y = box[a].y + Math.abs(box[a].y-box[b].y)/2;
				}
				if(box[a].y == box[b].y) {
					textKanten[aktKanten].y = box[a].y + 20;
				}

				textKanten.textBaseline = "alphabetic";
				stage.addChild(textKanten[aktKanten]);

				aktKanten++;
			}
		}
	}
}

/*
	Methode, für die Befärbung der Elemente
*/

function color(farbe){
	switch(farbe){
		case "gruen":
			return "green";
			break;
		case "gelb":
			return "yellow";
			break;
		case "blau":
			return "blue";
			break;
		case "rot":
			return "red";
			break;
		default:
			return "white";
			break;
	}
}
