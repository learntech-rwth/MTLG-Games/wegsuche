/*
	Dijkstra-Algorithmus bekommt als Argument ein zwei-dimensionales Array mit den
	entsprechenden Kantengewichten und der dazugehörigen Anzahl der Knoten
*/

function dijkstra(start, ziel, array, vertexes) {
		var start = start;
		var end = ziel;
		
		/*
			Das folgende Array speichert ab, ob ein Knoten markiert wurde,
			also ob er schon betrachtet wurde
		*/
		
		var markiert = new Array(vertexes);
		for(var i = 0; i < vertexes; i++) {
			markiert[i] = false;
		}
		
		/*
			Das folgende Array speichert die minimale Distanz vom Anfang zu dem Knoten ab
			Dazu muss genügend hoher Wert vorliegen
		*/
		
		var distance = new Array(vertexes);
		for(var i = 0; i < vertexes; i++) {
			distance[i] = 5000;
		}

		/*
			Speichere die Vorgänger
		*/
		
		var predecessor = new Array(vertexes);
		for(var i = 0; i < vertexes; i++) {
			predecessor[i] = i;
		}

		/*
			Initialisierung vom Beginn:
		*/
		
		distance[start] = 0;
		
		/* 
			Betrachteter Knoten 
		*/
		
		var currentVertex = start;
		
		while(existsUnmarkedVertex(markiert, vertexes)) {
			try {
				for(var j = 0; j < vertexes; j++) {
					if(distance[j] > (distance[currentVertex] + array[currentVertex][j]) && array[currentVertex][j] != 0) {
						distance[j] = (distance[currentVertex] + array[currentVertex][j]);
						predecessor[j] = currentVertex;
					}
				}
			} catch(err) {
				alert("Exception: " + err.message);
			} 
			
			markiert[currentVertex] = true;

			var next = nextVertex(distance, vertexes, markiert);
			if(next == -1) {
				break;
			}
			currentVertex = next;
		}
		return distance[end];
	}

	/*
		Hilfsmethoden für Dijkstra:
	*/

	function existsUnmarkedVertex(array, vertexes) {
		for(var i = 0; i < vertexes; i++) {
			if(array[i] == false) {
				return true;
			}
		}
		return false;
	}
	
	/*
		Hilfsmethode, wählt den nächsten nicht-markierten Knoten aus
	*/

	function nextVertex(distances, vertexes, marked) {
		var minimum = 5000;
		var next = -1;
		for(var i = 0; i < vertexes; i++) {
			if(marked[i] == false && distances[i] < minimum) {
				minimum = distances[i];
				next = i;
			}
		}
		return next;
	}