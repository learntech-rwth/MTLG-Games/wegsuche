var MTLG=(function(m){
  var options = {
    "languages":["en","de"], //Supported languages. First language should be the most complete.
    "countdown":180, //idle time countdown
    "fps":"60", //Frames per second
    "spielerzahl":4, //number of players
    "FilterTouches": true, //Tangible setting: true means a tangible is recognized as one touch event. False: 4 events.
  }

  m.loadOptions(options);
  return m;
})(MTLG);
